 General = {

  init: function() {
    console.log('GENERAL INIT');

    $(".menu-trigger").click(function(e){
      e.preventDefault();
      $(this).toggleClass('active');
      $("body").toggleClass('menu-mobile');
      $("#cbc-header").toggleClass('open');
    });

    $("#cbc-header-social-share").jsSocials({
      showLabel: false,
      showCount: false,
      shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest"]
    });

    $(".ico-share > a").click(function(e){
      e.preventDefault();
    });

    $(window).scroll(function(){
      var scrollPos = $(document).scrollTop();
      if(scrollPos>100){
        $("#cbc-header").addClass("compact");
      } else {
        $("#cbc-header").removeClass("compact");
      }
    });
  },

  getCookie: function (cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  },

  setCookie: function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  },

  isMobile: function () {
    if ($(window).width()<768){
      return "mobile";
    } else {
      return "desktop";
    }
  }

};

Home = {

  init: function(){
    this.initSwiper();
  },
  initSwiper: function(){
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 0,
      centeredSlides: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });
  }
};

Franchise = {

  init: function(){
    if($(".form-error-message").length){
      $('html, body').animate({
        scrollTop: $(".form-default").offset().top-160
      }, 500);
    }

    $(".form-franchise").submit(function(){
      $(".loading").addClass("show");
    });

    this.initMasks();
    this.initSelect2();
    //this.initCityCombo();
  },
  initCityCombo: function(){
    var states = $('#appbundle_lead_state');
    var cities = $('#appbundle_lead_city');

    if (states && cities) {
        states.on('change', function(){
            cities.children('optgroup').prop('disabled', true).hide();

            $(this).children('option:selected').each(function(){
                cities.children('optgroup[label="' + $(this).text() + '"]').prop('disabled', false).show();
            });

            cities.val(null).trigger('change');
        });

        states.trigger('change');
    }
  },
  initMasks: function() {
    var phoneMasks = ['(99) 9999-9999', '(99) 99999-9999'];
    var phoneInputs = $("#appbundle_lead_phonePrimary, #appbundle_lead_phoneSecondary");

    VMasker(phoneInputs).maskPattern(phoneMasks[0]);
    VMasker($("#appbundle_lead_cpf")).maskPattern("999.999.999-99");
    VMasker($("#appbundle_lead_cep")).maskPattern("99999-999");

    phoneInputs.on('input', function(e){
      var input = e.target;
      var raw = input.value.replace(/\D/g, '');
      var mask = input.value.length > phoneMasks[0].length ? phoneMasks[1] : phoneMasks[0];

      VMasker(input).unMask();
      VMasker(input).maskPattern(mask);
      input.value = VMasker.toPattern(raw, mask);
    });
  },
  initSelect2: function() {
    $('#appbundle_lead_city').select2();
    $('#appbundle_lead_cityOfInterestPrimary').select2();
    $('#appbundle_lead_cityOfInterestSecondary').select2();
    $('#appbundle_lead_investmentRange').select2();
  }
};

Contact = {

  init: function(){
    this.initFaqAccordion();
    this.initSelect2();
    this.initMasks();
    this.initBinds();

    console.log('Contact');
  },
  initFaqAccordion: function(){
    $('.perguntas-frequentes dl dt').on('click', function(e){
      $(this).toggleClass('open');
    });
  },
  initSelect2: function() {
    $('.select2').select2();
  },
  initMasks: function() {
    VMasker($("#appbundle_contact_mobile")).maskPattern("(99) 99999-9999");
    VMasker($("#appbundle_contact_phone")).maskPattern("(99) 9999-9999");
  },
  initBinds: function(){
    $('#form-check-forgot-password').prop('checked', false);
    $('#form-loading').hide();
    $('#form-signin').show();

    $('#form-show-signup').on('click', function(e){
      $('#form-signin').hide();
      $('#form-signup').show();
    });

    $('#form-show-signin').on('click', function(e){
      $('#form-signup').hide();
      $('#form-signin').show();
    });

    $('#form-forgot-password').on('click', function(e){
      $('#form-check-forgot-password').prop('checked', true);
      $('#form-signin').submit();
      $('#form-check-forgot-password').prop('checked', false);
    });

    $('#form-login-submit').on('click', function(e){
      $('#form-check-forgot-password').prop('checked', false);
    });
  }
};

Rule = {

  init: function(){
    this.initFaqAccordion();
  },
  initFaqAccordion: function(){
    $('.perguntas-frequentes dl dt').on('click', function(e){
      $(this).toggleClass('open');
    });
  }
};

Store = {

  init: function(){
    this.initSelect2();
    this.initSideMenuScrollBar();
    this.initSideMenuToggle();
  },
  initSelect2: function() {
    $('.select2').select2();
  },
  initSideMenuScrollBar: function() {
    $("#mapa-listing-enderecos").mCustomScrollbar("destroy").mCustomScrollbar({theme:"light"});
  },
  initSideMenuToggle: function(){
    $('.sideBar .drawer').on('click', function(e){
      $(".sideBar").toggleClass('open');
    });
  }
};

Product = {

  init: function(){
    this.initInfoAccordion();
  },
  initInfoAccordion: function(){
    $('.information h4').on('click', function(e){
      $(this).toggleClass('open');
    });
  }
};
