 General = {

  init: function() {
    console.log('GENERAL INIT');

    navigator.geolocation.getCurrentPosition( function( position ) {
      console.log( 'geolocation' );
    } );

    $(".menu-trigger").click(function(e){
      e.preventDefault();
      $(this).toggleClass('active');
      $("body").toggleClass('menu-mobile');
      $("#cbc-header").toggleClass('open');
    });

    $("#marketing-header-social-share, #marketing-header-social-share-2").jsSocials({
      showLabel: false,
      showCount: false,
      shares: ["facebook", "googleplus", "linkedin", "pinterest"]
    });

    $(".ico-share > a").click(function(e){
      e.preventDefault();
    });

    $(window).scroll(function(){
      var scrollPos = $(document).scrollTop();
      if(scrollPos>100){
        $("#marketing-header").addClass("compact");
      } else {
        $("#marketing-header").removeClass("compact");
      }
    });
  },

  getCookie: function (cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  },

  setCookie: function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  },

  isMobile: function () {
    if ($(window).width()<768){
      return "mobile";
    } else {
      return "desktop";
    }
  }

};

Home = {
  init: function(){
    this.initSwiper();
  },
  initSwiper: function(){
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 0,
      centeredSlides: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });
  }
};

Marketing = {
  init: function(){
    this.initSwiper();
  },
  initSwiper: function(){
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 0,
      centeredSlides: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });
  }
};

Contact = {
  init: function(){
    this.initFaqAccordion();
    this.initSelect2();
    this.initMasks();
    this.initBinds();
    this.initScroll();

    console.log('Contact');
  },
  initFaqAccordion: function(){
    $('.perguntas-frequentes dl dt').on('click', function(e){
      $(this).toggleClass('open');
    });
  },
  initSelect2: function() {
    $('.select2').select2();
  },
  initMasks: function() {
    VMasker($("#appbundle_contact_mobile")).maskPattern("(99) 99999-9999");
    VMasker($("#appbundle_contact_phone")).maskPattern("(99) 9999-9999");

    function inputHandler(masks, max, event) {
      var c = event.target;
      var v = c.value.replace(/\D/g, '');
      var m = c.value.length > max ? 1 : 0;
      VMasker(c).unMask();
      VMasker(c).maskPattern(masks[m]);
      c.value = VMasker.toPattern(v, masks[m]);
    }

    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('#appbundle_lead_phonePrimary');
    VMasker(tel).maskPattern(telMask[0]);
    tel.addEventListener( 'input', inputHandler.bind(undefined, telMask, 14), false);
    tel.addEventListener( 'change', inputHandler.bind(undefined, telMask, 14), false);
  },
  initBinds: function(){
    $('#form-check-forgot-password').prop('checked', false);
    $('#form-loading').hide();
    $('#form-signin').show();

    $('#form-show-signup').on('click', function(e){
      $('#form-signin').hide();
      $('#form-signup').show();
    });

    $('#form-show-signin').on('click', function(e){
      $('#form-signup').hide();
      $('#form-signin').show();
    });

    $('#form-forgot-password').on('click', function(e){
      $('#form-check-forgot-password').prop('checked', true);
      $('#form-signin').submit();
      $('#form-check-forgot-password').prop('checked', false);
    });

    $('#form-login-submit').on('click', function(e){
      $('#form-check-forgot-password').prop('checked', false);
    });
  },
  initScroll: function() {

    window.onload = function() {

      if( window.location.pathname.indexOf( 'sucesso' ) >= 0 || window.location.pathname.indexOf( 'erro' ) >= 0 || $( '.invalid-feedback' ).length > 0 ) {

        $( 'html, body' ).animate( {
            scrollTop: $( '#marketing-contact' ).offset().top
        }, 500);

      }

    }

  }
};

Store = {

  init: function(){
    this.initSelect2();
    this.initSideMenuScrollBar();
    this.initSideMenuToggle();
  },
  initSelect2: function() {
    $('.select2').select2();
  },
  initSideMenuScrollBar: function() {
    $("#mapa-listing-enderecos").mCustomScrollbar("destroy").mCustomScrollbar({theme:"light"});
  },
  initSideMenuToggle: function(){
    $('.sideBar .drawer').on('click', function(e){
      $(".sideBar").toggleClass('open');
    });
  }
};


Tagging = {
  init: function() {

    this.events();

  },
  events: function() {

    // exemplo
    //                      -> category      -> action       -> label    ->value
    // ga( 'send', 'event', 'redes-sociais', 'link-externo', 'facebook', '4' );

    /**************************/
    /********* AREAS **********/
    /**************************/
    $( '.js-clk-ga' )
      .on( 'click', function( target ) {

        var $this = $( this );

        ga( 'send', 'event', $this.data( 'categoria' ), $this.data( 'action' ), $this.data( 'label' ), { 'hitCallback':
          function() {
            document.location = $this.attr( 'href' );
          }
        } );

        target.preventDefault();

      } );

    $( '.js-clk-ext-ga' )
      .on( 'click', function( target ) {

        var $this = $( this );

        ga( 'send', 'event', $this.data( 'categoria' ), $this.data( 'action' ), $this.data( 'label' ) );

      } );

    $( '.js-sel-ga' )
      .on( 'change', function( target ) {

        var $this = $( this );

        ga( 'send', 'event', $this.data( 'categoria' ), $this.data( 'action' ), $this.find( 'option:selected' ).data( 'label' ) );

        target.preventDefault();

      } );

  }

};
