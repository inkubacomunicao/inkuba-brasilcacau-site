$(document).ready(function(){

    //carrega o form de places
    loadPlaces();

    // $mapa_listing = $('#mapa-listing');
    $mapa_listing_enderecos = $('#mapa-listing-enderecos');
    $mapa_full = $('#mapa-full');
    $gmaps = $('#gmaps');
    $gmaps_control = $('#gmaps_control');

    $lojas_ufs = $('#lojas_ufs');
    $lojas_cidades = $('#form_city');

    // armazenas os options de cidade
    $lojas_cidades_cache = $lojas_cidades.children('option').clone();

    $gmaps_markers = {};  //objeto para relacionar marcadores aos ids dos elementos na lista de lojas

    $gmaps.gmap({
        center: '-23.5489,-46.6388',  //posicao central default: são paulo / sp
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        zoom: 16
    }).bind('init', function(ev, map) {
        rand=99;
        $($mapa_listing_enderecos).find('li').each(function(){
            self = this;
            //gera uma pequena diferença nas coordenadas para evitar sobreposição nos marcadores (em lojas no mesmo shopping por ex)
            rand = parseInt((rand+(Math.random()*100))/2);

            var lat = ''+parseFloat($(self).attr('data-lat')).toFixed(4);

            rand = parseInt((rand+(Math.random()*100))/2);

            var lng = ''+parseFloat($(self).attr('data-lng')).toFixed(4);

            // if( $(self).attr('data-lat') == '-23.562691' && $(self).attr('data-lng') == '-46.655136' ){
            //     $icone = '/assets/site/img/store/' + 'pin-bigloja.png';
            // } else {
                $icone = '/assets/site/img/store/' + 'pin-brasil-cacau.png';
            // }

            //adiciona marcador
            $gmaps.gmap('addMarker',
                {
                    'position': lat+''+rand+','+lng+''+rand,
                    'bounds': false,
                    'icon': $icone,
                    'lojaId' : $(self).attr('data-id')
                },
                function(map, marker){
                    //adiciona referência do id da loja com o marcador, para exibir detalhes do marcador ao clicar no item na lista de lojas
                    $gmaps_markers['m_'+marker.lojaId]=marker;
                }
            ).click(function() {
                lojaOpenInfoWindow(this);
            });

        });

        refreshClusterer();
        resizeMapaFull();

        //centraliza o mapa no atributo de endereço (parseado a partir do $_GET['loja-search'])
        var dataAddress = $mapa_full.attr('data-address');
        var searchCoordinates = $mapa_full.attr('data-search-coords');

        //Verifica se existe um endereço preenchido para centralizar o mapa
        if((typeof dataAddress!='undefined') && (dataAddress.length)){
            //Verifica antes se não existe uma coordenada retornada do auto-complete, caso existir usa a coordenada ao invés do endereço
            if ( (typeof searchCoordinates!='undefined') && ( searchCoordinates.length ) ){

                //Quebra as coordenadas em latitude, longitude
                searchCoordinates = searchCoordinates.replace('(','').replace(')','').replace('+','').split(',');

                var myLatlng = new google.maps.LatLng(searchCoordinates[0], searchCoordinates[1]);

                var zoomLevel = 15;
                $gmaps.gmap('option', 'zoom', zoomLevel);
                $gmaps.gmap('option', 'center', ( myLatlng ) );
                updateLojasListing(true);

            } else {
                centerAddress( $mapa_full.attr('data-address'), false, 15);
                updateLojasListing(true);
            }

        }else{
            //se não, pede acesso a geolocalizacao do usuário
            $gmaps.gmap('getCurrentPosition', function(position, status){
                if ( status === 'OK' ) {
                    $gmaps.gmap('option', 'center', new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                    updateLojasListing(true);
                }
            });
        }

        updateLojasListing();

        //atualiza lista de lojas ao arrastar mapa
        $(gmaps_the_map).addEventListener('dragend', function(){
            $('#loja-search').val('');
            updateLojasListing();
        });

        //atualiza lista de lojas ao fazer zoom no mapa
        $(gmaps_the_map).addEventListener('zoom_changed', function(){
            updateLojasListing();
        });

        // $('#filtro-cidades').hide();

    });

    var gmaps_the_map = $gmaps.gmap('get', 'map');
    $(gmaps_the_map).addEventListener('tilesloaded', function(){
        if( $gmaps_control.length === 0 ){
            $('div.gmnoprint[controlwidth]:eq(0)').wrap('<div id="gmaps_control" class="box-gmaps-control" />');
        }
    });

    //seleciona UF, atualiza lista de cidades
    $lojas_ufs.change(function(){
        selUf = $(this).find('option:selected').val();
        updateCidadesListing( selUf );
    });

    // seleciona cidade, move centro do mapa para a cidade
    $lojas_cidades.change(function(){
        var address = $(this).find('option:selected').html() + ' - ' + $(this).find('option:selected').parent('optgroup').attr('label') + ' - Brasil';
        centerAddress(address, false);
        $mapa_listing_enderecos.find('li').hide();
        $mapa_listing_enderecos.find('li[data-cidade="'+ $(this).find('option:selected').val() +'"]').show();
    });

    //Centraliza Mapa ao clicar em uma das lojas na barra lateral.
    $mapa_listing_enderecos.find('li').on('click', function(){
        var myLatlng = new google.maps.LatLng($(this).attr('data-lat'), $(this).attr('data-lng'));
        var zoomLevel = 15;
        $gmaps.gmap('option', 'zoom', zoomLevel);
        $gmaps.gmap('option', 'center', ( myLatlng ) );
        updateLojasListing(true);
    });

    //exibe detalhes ao clicar na lista de lojas
    $mapa_listing_enderecos.find('li').on('click', function(){
        var lojaId = $(this).attr('data-id');
        if(typeof $gmaps_markers['m_'+lojaId]!='undefined'){
            lojaOpenInfoWindow($gmaps_markers['m_'+lojaId]);
        }

    });

    //busca por endereço, centraliza o mapa
    $('#form-lojas button').click(function(e){
        e.preventDefault();
        centerAddress($('#loja-search').val(), true, 14);
    });


    $("#form-lojas").submit(function (e) {
        e.preventDefault();
    });

    //Listener para centralizar o mapa caso o usuário clique em um item do auto complete.
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        zoomLevel = 15;
        $gmaps.gmap('option', 'zoom', zoomLevel);
        $gmaps.gmap('option', 'center', (place.geometry.location));
        updateLojasListing(true);
        $("#row-filtro-endereco").hide();
        $("span.bot-gen").removeClass( "ativo" );
    });

});

$(window).resize(function(){
    resizeMapaFull();
});

function resizeMapaFull(){
    $gmaps.gmap('refresh');
}

function refreshClusterer(){
    $gmaps.gmap('set', 'MarkerClusterer',
        new MarkerClusterer(
            $gmaps.gmap('get', 'map'),
            $gmaps.gmap('get', 'markers'),
            {
                maxZoom: 40,
                gridSize: 10,
                zoom: 10,
                styles: [
                    {
                        url: '/assets/site/img/store/' + 'clusterer-30.png',
                        height: 27,
                        width: 30,
                        anchor: [3, 0],
                        textColor: '#ff00ff',
                        textSize: 10
                    }, {
                        url: '/assets/site/img/store/' + 'clusterer-40.png',
                        height: 36,
                        width: 40,
                        anchor: [6, 0],
                        textColor: '#ff0000',
                        textSize: 11
                    }, {
                        url: '/assets/site/img/store/' + 'clusterer-50.png',
                        height: 50,
                        width: 45,
                        anchor: [8, 0],
                        textColor: '#ffffff',
                        textSize: 12
                    }
                ]
            }
        )
    );
}

//abre detalhes da loja
function lojaOpenInfoWindow(marker){
    var lojaId=marker.lojaId;
    var elem=$($mapa_listing_enderecos).find('[data-id="'+lojaId+'"]');
    var lojaTitle=$(elem).find('h2').text();
    var lojaContent=$(elem).attr('data-desc')+
        '<span class="btn-rota"><a href="https://maps.google.com.br/maps?f=d&daddr='+marker.position+'&z=13" target="_BLANK">Traçar Rota</a></span>';

    //atualiza elemento ativo
    $('.mapa-listing-enderecos ul li').removeClass('active');
    $('.mapa-listing-enderecos ul li[data-id="'+lojaId+'"]').addClass('active');

    //abre detalhes (infoWindow), centraliza e aumenta o zoom
    $gmaps.gmap('openInfoWindow', {'content': '<strong>'+lojaTitle+'</strong><br />'+lojaContent }, marker);
    $gmaps.gmap('option', 'center', marker.position);
    $gmaps.gmap('option', 'zoom', 22);
}

//atualiza lista de cidades a partir do estado selecionado
function updateCidadesListing( selUf ){
    $lojas_cidades.children('option').remove();
    $lojas_cidades.html('<option value="">Cidades</option>');
    $lojas_cidades_cache.clone().filter('[data-filtro-parent="'+ selUf +'"]').appendTo($lojas_cidades);
    $lojas_cidades.val('');
}

//atualiza lista de lojas a partir dos marcadores visíveis no mapa
//se autozoom=true, chama a função autoZoomOut()

function updateLojasListing(autozoom){

    if(typeof autozoom=='undefined') autozoom = false;
    var map = $gmaps.gmap('get', 'map');
    var bounds = map.getBounds();

    $mapa_listing_enderecos.find('li').each(function(){
        var marker = $gmaps_markers['m_'+$(this).attr('data-id')];

        if(bounds.contains(marker.position)){
            var distanceFromCenter = parseInt(google.maps.geometry.spherical.computeDistanceBetween(map.getCenter(), marker.position));
            $(this).attr('data-order', distanceFromCenter);
            $(this).show();
        }else{
            $(this).hide();
        }
    }).sort(function(a, b) {
        first = parseInt(a.dataset.order) || Number.MAX_SAFE_INTEGER;
        second = parseInt(b.dataset.order) || Number.MAX_SAFE_INTEGER;

        return +first - +second;
    }).appendTo($mapa_listing_enderecos);

    Store.initSideMenuScrollBar();

    if(autozoom){
        if(!$mapa_listing_enderecos.find('li:visible').length){
            autoZoomOut();
        }
    }

}

//mapa recebe zoomOut até que pelo menos uma loja fique visível
function autoZoomOut(){
    if(!$($mapa_listing_enderecos).find('li:visible').length){
        var curZoom=$gmaps.gmap('option', 'zoom');

        if(curZoom>5){
            $gmaps.gmap('option', 'zoom', curZoom-1);
            updateLojasListing(true);
            //autoZoomOut();
        }
    }
}

//faz busca por endereco e centraliza o mapa
function centerAddress(address, showAlertIfError, zoomLevel){
    //saddress='Bourbon Shopping - Rua Turiassu, São Paulo, Brasil';
    if(address.length){
        if(!zoomLevel) zoomLevel = 12;
        var geocoder = new google.maps.Geocoder();
        // var c7_zoom_level = 12;
        // if( $.trim(address).length ==  )
        geocoder.geocode( { "address": address, 'region': 'BR'}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $($mapa_listing_enderecos).find('li:visible').hide();
                $gmaps.gmap('option', 'zoom', zoomLevel);
                $gmaps.gmap('option', 'center', (results[0].geometry.location));
                updateLojasListing(true);
            } else if(showAlertIfError){
                alert("Não foi possível localizar: " + status);
            }
        });
    }
}

function loadPlaces(){

    var input = document.getElementById('loja-search');

    var options = {
        componentRestrictions: {country: 'br'}
    };

    autocomplete = new google.maps.places.Autocomplete(input, options);


    //Listener para centralizar o mapa caso o usuário clique em um item do auto complete.
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        var coordenadas = document.getElementById('search-coordinates');
        coordenadas.value = place.geometry.location;
    });
}
