var Product = (function(self){
	var init = function(){
		bind();
	}

	var bind = function(){
        bindDatatableDrawEvent();
        bindFeatherlight();
        bindImageSelector();
	}

    var bindDatatableDrawEvent = function(){
        if($.fn.DataTable != undefined) {
            var interval = window.setInterval(function() {
                if ($.fn.DataTable.tables().length > 0) {

                    jQuery(window).on('draw.dt', function() {
                        bindFeatherlight();
                    });

                    clearInterval(interval);
                }
            }, 10);
        }
    }

    var bindFeatherlight = function(){
        jQuery('[data-featherlight]').featherlight();
    }

	var bindImageSelector =  function (){
		jQuery("#appbundle_product_relatedProducts").select2({
			allowClear: true,
			templateResult: function (option) {
                var regex = new RegExp(/<!--\s(.*)\s-->\s(.*)/);
                var matches = regex.exec(option.text);

                return matches && matches.length == 3 ? matches[2] : option.text;
			},
			templateSelection: function (option) {
                var regex = new RegExp(/<!--\s(.*)\s-->\s(.*)/);
                var matches = regex.exec(option.text);

                return matches && matches.length == 3 ? '<img src="' + matches[1] + '" /> ' + matches[2] : option.text;
			},
			escapeMarkup: function (m) {
				return m;
			}
		});
	}    
	
	return {
		init: init
	};
})();

jQuery(window).on('load', function(){
    Product.init();
});
