var Lead = (function(){
	var init = function(){
		bind();
	}

	var bind = function(){
		bindConfirmButtons();
	}

	var bindConfirmButtons = function(){
		jQuery('form[action$="/defer"], form[action$="/reject"], form[action$="/implement"]').on('submit', function(e){
			return confirm("Tem certeza que deseja alterar o status desta solicitação? Uma notificação de atualização será enviada ao solicitante por e-mail.")
		});
	}

	return {
		init: init
	};
})();

jQuery(window).on('load', function(){
    Lead.init();
});