var Store = (function(self){
    var init = function(){
        bind();
    }

    var bind = function(){
        bindAddress();
        fetchLocation();
        bindInputMask();
    }

    var googleApiKey = jQuery('#store_script').data('google_api_key');

    var bindInputMask = function() {
        jQuery('[data-inputmask]').inputmask();
    }

    var fetchLocation = function() {
        var address = jQuery('#appbundle_store_address').val() + ', ' + jQuery('#appbundle_store_city').find(':selected').text();
        jQuery.ajax({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + googleApiKey,
            type: 'GET',
            success: function(res) {
                jQuery("#appbundle_store_latitude").val(res.results[0].geometry.location.lat);
                jQuery("#appbundle_store_longitude").val(res.results[0].geometry.location.lng);
            }
        });
    }

    var bindAddress = function(){
        jQuery('#appbundle_store_address').blur(function() {
            fetchLocation();
        });

        jQuery('#appbundle_store_city').change(function() {
            fetchLocation();
        });
    }

    return {
        init: init
    };
})();

jQuery(window).on('load', function(){
    Store.init();
});