var General = (function(self){
	var init = function(){
		bind();
	}

	var bind = function(){
		bindDeleteButtonConfirm();
		bindNameAndSlugInput();
		bindInputMask();
		bindSelect2();
	}

	var bindDeleteButtonConfirm = function(){
		jQuery("input[type='hidden'][value='DELETE']").parent("form").on("submit", function(){
			return confirm("Are you sure you want to DELETE this record? This operation is IRREVERSIBLE.");
		});
	}

	var bindNameAndSlugInput = function(){
		var nameInput = $('input[id*="_name"]');
		var slugInput = $('input[id*="_slug"]');
        if (nameInput.length == 0)
            nameInput = $('input[id*="_text"]');
		
		if (nameInput.length == 1 && slugInput.length == 1 && slugInput.val().length == 0) {
			nameInput.on('change keyup', function(e){
				slugInput.val(General.slugfy(nameInput.val()));
			});

			slugInput.on('keyup', function(){
				nameInput.off('change keyup');
			});
		}
	}

	var bindInputMask = function(){
		jQuery("[data-inputmask]").inputmask();
	}

	var bindSelect2 = function(){
		jQuery(".select2").select2();
	}	

	var slugify = function(str) {
        return jQuery.trim(str).replace(/[^a-z0-9-]/gi, "-").replace(/-+/g, "-").replace(/^-|-$/g, "").toLowerCase();
	}	
	
	return {
		init: init,
		slugfy: slugify
	};
})();

$(document).ready(function(){
	General.init();
});