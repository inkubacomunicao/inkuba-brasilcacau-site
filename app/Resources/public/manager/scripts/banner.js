var Banner = (function(){
	var init = function(){
		bind();
	}

	var bind = function(){
		bindDatatableDrawEvent();
		bindFeatherlight();
		bindOrdenatorButtons();
		bindUrlSelect2();
		bindStatusButtons();
	}

	var bindDatatableDrawEvent = function(){
		if($.fn.DataTable != undefined) {
			var interval = window.setInterval(function() {
				if ($.fn.DataTable.tables().length > 0) {

					jQuery(window).on('draw.dt', function() {
						bindFeatherlight();
						bindOrdenatorButtons();
						bindStatusButtons();
					});
	
					clearInterval(interval);
				}
			}, 10);
		}
	}

	var bindFeatherlight = function(){
		jQuery('[data-featherlight]').featherlight();
	}

	var bindOrdenatorButtons = function(){
		jQuery('a.btn.ordenator').on('click', function(e){
			e.preventDefault();

			regex = /banner\/(\d+)\/edit/ig;
			matches = regex.exec(jQuery(this).attr('href'));
			id = matches && matches.length == 2 ? matches[1] : null;
			direction = jQuery(this).data('direction');

			if (id) {
				jQuery.ajax({
					success: function(data){
						jQuery('#sg-datatables-banner_datatable').DataTable().draw(false);
					},
					error: function(){
						alert("There was an error processing your request. Please, try again.")
					},
					processData: false,
					type: "POST",
					url: "/api/banner/" + id + "/ordenator/" + direction
				});  
			}
		});
	}	

	var bindStatusButtons = function(){
		jQuery('a.btn.status').on('click', function(e){
			e.preventDefault();

			regex = /banner\/(\d+)\/edit/ig;
			id = regex.exec(jQuery(this).attr('href'))[1];
			status = jQuery(this).data('status');

			jQuery.ajax({
                success: function(data){
                    jQuery('#sg-datatables-banner_datatable').DataTable().draw(false);
                },
                error: function(){
                    alert("There was an error processing your request. Please, try again.")
                },
                processData: false,
                type: "POST",
                url: "/api/banner/" + id + "/isactive/" + status
            });  
		});
	}	

	var bindUrlSelect2 = function(){
		jQuery(".select2tag").select2({
			tags: true
		});
	}		

	return {
		init: init
	};
})();

jQuery(window).on('load', function(){
    Banner.init();
});