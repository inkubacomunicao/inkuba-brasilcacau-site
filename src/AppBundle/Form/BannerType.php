<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Repository\CultureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BannerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
            ->add('desktopImage', FileType::class, [
                'required' => $options['requireUpload'],
                'label' => 'Imagem Desktop'
            ])
			->add('mobileImage', FileType::class, [
                'required' => $options['requireUpload'],
                'label' => 'Imagem Mobile'
            ])            
            ->add('url', null, [
                'label' => 'Link',
            ])          
            ->add('isActive', null, [
                'label' => 'Ativo',
            ])
            ->add('publishAt', null, [
                'label' => 'Data de Publicação',
            ])             
            ->add('expiresAt', null, [
                'label' => 'Data de Expiração',
            ]) 
			->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $banner = $event->getData();
                $form = $event->getForm();

                if (!$banner) {
                    return;
                }

                if ((!array_key_exists('mobileImage', $banner) || $banner['mobileImage'] == null) && null !== $form->get('mobileImage')->getData())
                {
                    $banner['mobileImage'] = $form->get('mobileImage')->getData();
                    $event->setData($banner);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $banner = $event->getData();
                $form = $event->getForm();

                if (!$banner) {
                    return;
                }

                if ((!array_key_exists('desktopImage', $banner) || $banner['desktopImage'] == null) && null !== $form->get('desktopImage')->getData())
                {
                    $banner['desktopImage'] = $form->get('desktopImage')->getData();
                    $event->setData($banner);
                }
            })
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Banner',
			'requireUpload' => true,			
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_banner';
    }


}
