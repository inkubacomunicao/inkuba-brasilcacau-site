<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Dealership;
use AppBundle\Repository\DealershipRepository;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username');

        if ($options['password']) {
            $builder
                ->add('passwordText', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Senhas não são iguais.',
                    'required' => false,
                    'first_name' => 'password',
                    'second_name' => 'confirm',
                    'first_options'  => ['label' => 'Senha', 'error_bubbling' => true],
                    'second_options' => ['label' => 'Confirme a senha'], 'error_bubbling' => true,
                    'mapped' => true])
            ;             
        }

        $builder->add('roles', ChoiceType::class, [
                    'label' => 'Perfis',
                    'mapped' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => [
                        'Manager' => 'ROLE_MANAGER',
                ]]) 
				->add('isActive', null, ['label' => 'Ativo'])
		;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'password' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
