<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\ProductRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ThemeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
			->add('text', null, ['label' => 'Tema'])
            ->add('slug')
            ->add('description', null, ['label' => 'Descrição'])
            ->add('textColor', null, ['label' => 'Cor do Texto (HEX)'])
            ->add('pageBackgroundImage', FileType::class, [
                'label' => 'Ilustração de Fundo',
                'required' => false,
            ])
            ->add('textBackgroundImage', FileType::class, [
                'label' => 'Gradiente de Fundo',
                'required' => false,
            ])
            ->add('isActive', null, ['label' => 'Ativa']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Theme'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_theme';
    }


}
