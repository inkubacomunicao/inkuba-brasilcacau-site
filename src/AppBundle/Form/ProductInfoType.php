<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\TagRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;



class ProductInfoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
            ->add('title', null, ['label' => 'Título'])
            ->add('ingredients', CKEditorType::class, ['label' => 'Ingredientes' ])
            ->add('nutritionFacts', CKEditorType::class, ['label' => 'Informação Nutricional'])
            ->add('isActive', null, ['label' => 'Ativo']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ProductInfo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product_info';
    }


}
