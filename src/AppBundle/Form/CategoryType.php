<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\ProductRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
			->add('text', null, ['label' => 'Categoria'])
            ->add('slug')
            ->add('description', null, ['label' => 'Descrição'])
            ->add('textColor', null, ['label' => 'Cor do Texto (HEX)'])            
            ->add('pageBackgroundImage', FileType::class, [
                'label' => 'Ilustração de Fundo',
                'required' => false,
            ])  
            ->add('textBackgroundImage', FileType::class, [
                'label' => 'Gradiente de Fundo',
                'required' => false,
            ])     
            ->add('titleBackgroundImage', FileType::class, [
                'label' => 'Imagem de Fundo do Titulo',
                'required' => false,
            ])  
            ->add('topLeftParallaxImage', FileType::class, [
                'label' => 'Adorno Flutuante (S.E.)',
                'required' => false,
            ])          
            ->add('bottomLeftParallaxImage', FileType::class, [
                'label' => 'Adorno Flutuante (I.E.)',
                'required' => false,
            ])       
            ->add('topRightParallaxImage', FileType::class, [
                'label' => 'Adorno Flutuante (S.D.)',
                'required' => false,
            ])     
            ->add('bottomRightParallaxImage', FileType::class, [
                'label' => 'Adorno Flutuante (I.D.)',
                'required' => false,
            ])    
            ->add('isFeatured', null, ['label' => 'Destacada no Menu'])                                                    
            ->add('isActive', null, ['label' => 'Ativa'])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('textBackgroundImage', $category) || $category['textBackgroundImage'] == null) && null !== $form->get('textBackgroundImage')->getData())
                {
                    $category['textBackgroundImage'] = $form->get('textBackgroundImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('pageBackgroundImage', $category) || $category['pageBackgroundImage'] == null) && null !== $form->get('pageBackgroundImage')->getData())
                {
                    $category['pageBackgroundImage'] = $form->get('pageBackgroundImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('titleBackgroundImage', $category) || $category['titleBackgroundImage'] == null) && null !== $form->get('titleBackgroundImage')->getData())
                {
                    $category['titleBackgroundImage'] = $form->get('titleBackgroundImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('topLeftParallaxImage', $category) || $category['topLeftParallaxImage'] == null) && null !== $form->get('topLeftParallaxImage')->getData())
                {
                    $category['topLeftParallaxImage'] = $form->get('topLeftParallaxImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('bottomLeftParallaxImage', $category) || $category['bottomLeftParallaxImage'] == null) && null !== $form->get('bottomLeftParallaxImage')->getData())
                {
                    $category['bottomLeftParallaxImage'] = $form->get('bottomLeftParallaxImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('topRightParallaxImage', $category) || $category['topRightParallaxImage'] == null) && null !== $form->get('topRightParallaxImage')->getData())
                {
                    $category['topRightParallaxImage'] = $form->get('topRightParallaxImage')->getData();
                    $event->setData($category);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $category = $event->getData();
                $form = $event->getForm();

                if (!$category) {
                    return;
                }

                if ((!array_key_exists('bottomRightParallaxImage', $category) || $category['bottomRightParallaxImage'] == null) && null !== $form->get('bottomRightParallaxImage')->getData())
                {
                    $category['bottomRightParallaxImage'] = $form->get('bottomRightParallaxImage')->getData();
                    $event->setData($category);
                }
            })            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Category'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_category';
    }


}
