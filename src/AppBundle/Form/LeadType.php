<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Lead;
use AppBundle\Entity\City;
use AppBundle\Entity\State;
use AppBundle\Repository\CityRepository;

class LeadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
              'attr' => [
                'oninvalid' => 'setCustomValidity(\'' . "Campo obrigatório." . '\');',
                'onchange' => 'setCustomValidity(\'\');'
              ],
            ])
            ->add('email')
            ->add('phonePrimary', null, [
                'attr' => [
                    'placeholder' => '(11) 99999-9999',
                    'required'  => true,
                    'oninvalid' => 'setCustomValidity(\'' . "Telefone inválido. Ex: (99) 9999-9999" . '\');',
                    'onchange' => 'setCustomValidity(\'\');'
                ],
            ])
            ->add('phoneSecondary', null, [
                'attr' => [
                    'placeholder' => '(11) 99999-9999',
                    'oninvalid' => 'setCustomValidity(\'' . htmlentities("Telefone inválido. Ex: (99) 9999-9999", ENT_QUOTES) . '\');',
                    'onchange' => 'setCustomValidity(\'\');'
                ],
            ])
            ->add('cpf')
            ->add('cep')
            ->add('address')
            ->add('number')
            ->add('complement')
            ->add('neighborhood')
            ->add('city', EntityType::class, [
                'attr' => [
                    'class' => 'select2'
                ],
                'class' => 'AppBundle:City',
                'query_builder' => function (CityRepository $er) {
                    return $er->createQueryBuilder('c')->join('c.state', 's')->addOrderBy('s.abbreviation', 'ASC')->addOrderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'group_by' => function ($val, $key, $index) {
                    return $val->getState()->getAbbreviation();
                },
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'placeholder' => 'Selecione Cidade',
            ])
            ->add('hasAgreed', null, [
                'label'     => 'Aceito receber as novidades e promoções.',
                'required'  => true
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Lead',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_lead';
    }
}
