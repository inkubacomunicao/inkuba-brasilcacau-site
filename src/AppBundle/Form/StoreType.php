<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\CityRepository;

class StoreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //TODO: Implementar Maps API -> http://maps.google.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA
        $builder
            ->add('name', null, ['label' => 'Nome'])
            ->add('address', null, ['label' => 'Endereço'])
            ->add('addressComplement', null, ['label' => 'Complemento'])
            ->add('addressDistrict', null, ['label' => 'Bairro'])
            ->add('addressZipcode', null, ['label' => 'Cep', 'attr' => ['maxlength'=>'9', 'data-inputmask' => "'mask':'99999-999'"]])
            ->add('phoneNumber', null, ['label' => 'Telefone', 'attr' => ['maxlength'=>'16', 'data-inputmask' => "'mask':'(99) 9999[9]-9999'"]])
            ->add('whatsappNumber', null, ['label' => 'WhatsApp', 'attr' => ['maxlength'=>'16', 'data-inputmask' => "'mask':'(99) 99999-9999'"]])
            ->add('city', EntityType::class, [
                'attr' => [
                    'class' => 'select2'
                ],
                'label' => 'Cidade',
                'class' => 'AppBundle:City',
                'query_builder' => function (CityRepository $er) {
                    return $er->createQueryBuilder('c')->join('c.state','s')->addOrderBy('s.abbreviation', 'ASC')->addOrderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'group_by' => function($val, $key, $index) {
                    return $val->getState()->getAbbreviation();
                },
                'required' => true,
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('latitude')
            ->add('longitude')
            ->add('hasCoffee', null, ['label' => 'Tem cafeteria'])
            ->add('isActive', null, ['label' => 'Ativa']);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Store'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_store';
    }


}
