<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductInfoRepository;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\ThemeRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
            ->add('name', null, ['label' => 'Nome'])
            ->add('slug')
            ->add('description', null, ['label' => 'Descrição'])
            ->add('mobileImage', FileType::class, [
                'required' => $options['requireUpload'],
                'label' => 'Imagem desktop'
            ])
            ->add('desktopImage', FileType::class, [
                'required' => $options['requireUpload'],
                'label' => 'Imagem mobile'
            ])
            ->add('netWeight', null, ['label' => 'Peso Líq.'])
            ->add('theme', EntityType::class, [
                'label' => 'Tema',
                'attr' => [
                    'class' => 'select2'
                ],
                'class' => 'AppBundle:Theme',
                'query_builder' => function (ThemeRepository $er) {
                    return $er->createQueryBuilder('t')->where('t.isActive = 1')->orderBy('t.text', 'ASC');
                },
                'choice_label' => 'text',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('categories', EntityType::class, [
                'label' => 'Categorias',
                'attr' => [
                    'class' => 'select2'
                ],
                'class' => 'AppBundle:Category',
                'query_builder' => function (CategoryRepository $er) {
                    return $er->createQueryBuilder('t')->where('t.isActive = 1')->orderBy('t.text', 'ASC');
                },
                'choice_label' => 'text',
                'required' => false,
                'multiple' => true,
                'expanded' => false,
            ])
            ->add('productInfos', EntityType::class, [
                'label' => 'Informações do produto',
                'attr' => [
                    'class' => 'select2'
                ],
                'class' => 'AppBundle:ProductInfo',
                'query_builder' => function (ProductInfoRepository $er) {
                    return $er->createQueryBuilder('t')->where('t.isActive = 1')->orderBy('t.title', 'ASC');
                },
                'choice_label' => 'title',
                'required' => false,
                'multiple' => true,
                'expanded' => false,
            ])
            ->add('relatedProducts', EntityType::class, [
			    'label' => 'Produtos Relacionados',
				'attr' => [
					'class' => 'select2'
				],					
                'class' => 'AppBundle:Product',
                'query_builder' => function (ProductRepository $er) {
                    return $er->createQueryBuilder('c')->where('c.isActive = 1')->orderBy('c.name', 'ASC');
                },
				'choice_label'  => function ($product) use ($options) {
                    if ($options['imagine'] && $options['imagePath'] && $product->getDesktopImage()) {
                        $filename = is_string($product->getDesktopImage()) ? $product->getDesktopImage() : $product->getDesktopImage()->getFilename();

                        return sprintf(
                            '<!-- %s --> %s',
                            $this->getThumbForImage($filename, $options['imagine'], $options['imagePath']), 
                            $product->getName()
                        );
                    } else {
                        return $product->getName();
                    }
				},                                 
                'required' => false,
                'multiple' => true,
                'expanded' => false,              
			])
            ->add('isActive', null, ['label' => 'Ativo'])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $product = $event->getData();
                $form = $event->getForm();

                if (!$product) {
                    return;
                }

                if ((!array_key_exists('mobileImage', $product) || $product['mobileImage'] == null) && null !== $form->get('mobileImage')->getData())
                {
                    $product['mobileImage'] = $form->get('mobileImage')->getData();
                    $event->setData($product);
                }
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $product = $event->getData();
                $form = $event->getForm();

                if (!$product) {
                    return;
                }

                if ((!array_key_exists('desktopImage', $product) || $product['desktopImage'] == null) && null !== $form->get('desktopImage')->getData())
                {
                    $product['desktopImage'] = $form->get('desktopImage')->getData();
                    $event->setData($product);
                }
            });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product',
            'requireUpload' => true,
            'imagine' => null,
			'imagePath' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }

	private function getThumbForImage($filename, $imagine, $imagePath)
	{
		if (!is_null($imagine) && !is_null($imagePath)) 
		{
			return $imagine->getBrowserPath($imagePath . '/' . $filename, 'manager_datatable_thumb');
		}
		else
		{
			throw new \Exception('Missing Liip Imagine or Image Path options param in FormType.');
		}
	}
}
