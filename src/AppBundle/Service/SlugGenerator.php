<?php

namespace AppBundle\Service;

class SlugGenerator
{
	public function slugify($string) {
		$search = explode(",","ç,æ,œ,ã,õ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u");
		$replace = explode(",","c,ae,oe,a,o,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u");
		$string = str_replace($search, $replace, $string);

		$string = strtolower($string);
		$string = preg_replace('/[^a-z0-9-]/', '-', $string);
		$string = preg_replace('/-+/', '-', $string);
		$string = preg_replace('/^-|-$/', '', $string);

		return $string;
	}
}
