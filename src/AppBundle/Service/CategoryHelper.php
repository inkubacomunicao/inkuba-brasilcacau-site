<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

/**
 * Class CategoryHelper
 *
 * @package AppBundle\Service
 */
class CategoryHelper
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getActiveWithFeatured()
    {
        return $this->em->getRepository("AppBundle:Category")->findBy(['isActive' => true],['isFeatured' => 'DESC', 'text' => 'ASC']);
    }
}

