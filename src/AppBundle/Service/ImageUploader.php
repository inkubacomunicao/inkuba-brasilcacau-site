<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ImageUploader
 *
 * @package AppBundle\Service
 */
class ImageUploader
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function save($file)
    {
        if(!file_exists($this->path))
            mkdir($this->path, 0777, true);

        if($file instanceof Symfony\Component\HttpFoundation\File\File && file_exists($file->getPathname()))
            return;

        $extension = $file->guessExtension();

        $uniqueName = uniqid('chocolates_brasil_cacau_', true);

        $filename = $uniqueName . '.' . $extension;

        $file->move(
            $this->path,
            $filename
        );

        return $filename;
    }

    public function retrieve($filename) {
        $fullPath =  $this->path . DIRECTORY_SEPARATOR . $filename;

        return file_exists($fullPath) && null !== $filename ? new File($fullPath) : null;
    }
}

