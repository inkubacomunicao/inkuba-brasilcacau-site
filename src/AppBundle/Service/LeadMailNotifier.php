<?php

namespace AppBundle\Service;

use Symfony\Bundle\TwigBundle\TwigEngine;

class LeadMailNotifier
{
    protected $mailFrom;
    protected $templating;
    protected $mailer;

    public function __construct(TwigEngine $templating, \Swift_Mailer $mailer, $mailFrom)
    {
        $this->mailFrom = $mailFrom;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    public function send($lead)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[Brasil Cacau] Novo LEAD recebido: ' . $lead->getName())
            ->setFrom($this->mailFrom)
            ->setTo($this->mailFrom)
            ->setBody($this->templating->render('emails/lead_system_notification.html.twig', ['lead' => $lead]), 'text/html');
        
        $this->mailer->send($message);

        $message = \Swift_Message::newInstance()
            ->setSubject('[Brasil Cacau] Seu cadastro de franquiado foi recebido.')
            ->setFrom($this->mailFrom)
            ->setTo($lead->getEmail())
            ->setBody($this->templating->render('emails/lead_user_notification.html.twig', ['lead' => $lead]), 'text/html');
        
        $this->mailer->send($message);
    }

    public function sendUpdate($lead)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[Brasil Cacau] Seu cadastro de franquiado foi atualizado.')
            ->setFrom($this->mailFrom)
            ->setTo($lead->getEmail())
            ->setBody($this->templating->render('emails/lead_user_update_notification.html.twig', ['lead' => $lead]), 'text/html');
        
        $this->mailer->send($message);
    }    
}
