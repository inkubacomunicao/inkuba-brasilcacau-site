<?php

namespace AppBundle\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends Controller
{
    /**
     * @Route("/quem-somos", name="site_about_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('site/about/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/politica-de-privacidade", name="site_about_privacy")
     */
    public function privacyAction(Request $request)
    {
        return $this->render('site/about/privacy.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/regulamento", name="site_about_rule")
     */
    public function ruleAction(Request $request)
    {
        return $this->render('site/about/rule.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/conecte-se", name="site_about_social")
     */
    public function conecteseAction(Request $request)
    {
        return $this->render('site/about/conecte-se.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
