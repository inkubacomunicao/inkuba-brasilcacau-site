<?php

namespace AppBundle\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Lead;

class MarketingController extends Controller
{
  /**
   * @Route("/natal", name="martketing_natal_2020")
   */
  public function landingRedir(Request $request)
  {
    return $this->redirect('https://natal.chocolatesbrasilcacau.com.br');
  }

    /**
     * /natal martketing_natal_blackfriday
     */
    // public function landingBF2019Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('natal_2019');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if( $form->isSubmitted() && $form->isValid() ) {
    //
    //         try {
    //
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_error', $request->query->all());
    //
    //         }
    //
    //     }
    //
    //     $submitDataError = ( $form->isSubmitted() && $form->isValid() );
    //
    //     return $this->render('site/marketing/natal-blackfriday-2019/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnError' => false,
    //         'warnSuccess' => false,
    //         'submitDataError' => $submitDataError
    //     ]);
    // }

    /**
     * "/natal/sucesso martketing_natal_blackfriday_success
     */
    // public function successBF2019Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('natal_2019');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         try {
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_error', $request->query->all());
    //
    //         }
    //     }
    //
    //     return $this->render('site/marketing/natal-blackfriday-2019/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnSuccess' => true,
    //         'warnError' => false,
    //         'submitDataError' => false
    //     ]);
    // }

    /**
     * /natal/erro martketing_natal_blackfriday_error
     */
    // public function errorBF2019Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('natal_2019');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         try {
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_natal_blackfriday_error', $request->query->all());
    //
    //         }
    //     }
    //
    //     return $this->render('site/marketing/natal-blackfriday-2019/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnError' => true,
    //         'warnSuccess' => false,
    //         'submitDataError' => false
    //     ]);
    // }

    /**
     * @Route("/namorados", name="martketing_namorados")
     */
    // public function landingDN2020Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('dia_namorados_2020');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if( $form->isSubmitted() && $form->isValid() ) {
    //
    //         try {
    //
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //             return $this->redirectToRoute('martketing_namorados_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_namorados_error', $request->query->all());
    //
    //         }
    //
    //     }
    //
    //     $submitDataError = ( $form->isSubmitted() && $form->isValid() );
    //
    //     return $this->render('site/marketing/dia-namorados-2020/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnError' => false,
    //         'warnSuccess' => false,
    //         'submitDataError' => $submitDataError
    //     ]);
    // }

    /**
     * namorados/sucesso martketing_namorados_success
     */
    // public function successDN2020Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('dia_namorados_2020');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         try {
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //             return $this->redirectToRoute('martketing_namorados_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_namorados_error', $request->query->all());
    //
    //         }
    //     }
    //
    //     return $this->render('site/marketing/dia-namorados-2020/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnSuccess' => true,
    //         'warnError' => false,
    //         'submitDataError' => false
    //     ]);
    // }

    /**
     * namorados/erro martketing_namorados_error
     */
    // public function errorDN2020Action(Request $request)
    // {
    //     $lead = new Lead();
    //     $lead->setCampaign('dia_namorados_2020');
    //
    //     $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);
    //
    //     $form = $this->createForm('AppBundle\Form\LeadType', $lead);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         try {
    //             $em = $this->getDoctrine()->getManager();
    //             $em->persist($lead);
    //             $em->flush();
    //
    //             // $sender = $this->get(LeadMailNotifier::class);
    //             // $sender->send($lead);
    //
    //             return $this->redirectToRoute('martketing_namorados_success', $request->query->all());
    //
    //         } catch (\Exception $ex) {
    //
    //             return $this->redirectToRoute('martketing_namorados_error', $request->query->all());
    //
    //         }
    //     }
    //
    //     return $this->render('site/marketing/dia-namorados-2020/index.html.twig', [
    //         'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    //         'form' => $form->createView(),
    //         'stores' => $stores,
    //         'warnError' => true,
    //         'warnSuccess' => false,
    //         'submitDataError' => false
    //     ]);
    // }
}
