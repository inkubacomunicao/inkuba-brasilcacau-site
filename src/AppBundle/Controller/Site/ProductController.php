<?php

namespace AppBundle\Controller\Site;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use AppBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends Controller
{
    /**
     * @Route("/produtos", name="site_product_index")
     */
    public function indexAction(Request $request)
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)->createQueryBuilder('c')->where('c.isActive = 1')->orderBy('c.text', 'ASC')->getQuery()->getResult();

        return $this->render('site/product/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/categoria/{slug}", name="site_product_category")
     */
    public function categoryAction(Request $request, $slug)
    {
        $category =  $this->getDoctrine()->getRepository(Category::class)->findOneBy(['isActive' => true, 'slug' => $slug]);
        $products = $this->getDoctrine()->getRepository(Product::class)->findByCategory($category);

        return $this->render('site/product/category.html.twig', [
            'category' => $category,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/produto/{slug}", name="site_product_show")
     */
    public function showAction(Request $request, $slug)
    {
        $product =  $this->getDoctrine()->getRepository(Product::class)->findOneBy(['isActive' => true, 'slug' => $slug]);

        return $this->render('site/product/show.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/buscar", name="site_product_search")
     */
    public function searchAction(Request $request)
    {
        $term = $request->query->get('s');
        $products = $this->getDoctrine()->getRepository(Product::class)->findBySearchString($term, 24);

        return $this->render('site/product/search.html.twig', [
            'products' => $products,
            'term' => $term,
        ]);
    }    

    /**
     * @Route("/repositorio", name="site_product_repository")
     */
    public function repositoryAction(Request $request)
    {
        $products =  $this->getDoctrine()->getRepository(Product::class)->findBy(['isActive' => true], ['name' => 'ASC']);

        return $this->render('site/product/repository.html.twig', [
            'products' => $products
        ]);
    }    
}
