<?php

namespace AppBundle\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Banner;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="site_home_index")
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Banner::class);

        $banners = $repository->findDisplayable();

        return $this->render('site/default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'banners' => $banners
        ]);
    }

    /**
     * @Route("/homolog", name="site_home_homolog")
     */
    public function homologAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Banner::class);

        $banners = $repository->findDisplayable();

        return $this->render('site/default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'banners' => $banners
        ]);
    }
}
