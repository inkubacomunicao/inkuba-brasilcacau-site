<?php

namespace AppBundle\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\CityRepository;

class StoreController extends Controller
{
    /**
     * @Route("/mapa-lojas", name="site_store_index")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('city', EntityType::class, [
                'attr' => [
                    'class' => 'select2'
                ],
                'label' => 'Cidade',
                'class' => 'AppBundle:City',
                'query_builder' => function (CityRepository $er) {
                    return $er->createQueryBuilder('c')->join('c.state','s')->addOrderBy('s.abbreviation', 'ASC')->addOrderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'group_by' => function($val, $key, $index) {
                    return $val->getState()->getAbbreviation();
                },
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'placeholder' => 'Cidade',
            ])
            ->getForm();

        $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);

        return $this->render('site/store/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'stores' => $stores,
        ]);
    }
}
