<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * User controller.
 *
 * @Route("manager/user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/{filter}", name="manager_user_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.user');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			switch ($filter) {
				case 'active':
					$qb->andWhere('user.isActive = 1');
					break;
				case 'inactive':
					$qb->andWhere('user.isActive = 0');
					break;
			}
	
			return $responseService->getResponse();
		}

		return $this->render('manager/user/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="manager_user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user, ['password' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();

			try
			{			
				$em = $this->getDoctrine()->getManager();
				$user->setPassword($this->encodePassword($user));
				$em->persist($user);
				$em->flush();

				$session->getFlashBag()->add('success', 'Usuário gravado com sucesso.');

				return $this->redirectToRoute('manager_user_show', array('id' => $user->getId()));
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
			}        
		}

        return $this->render('manager/user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="manager_user_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('manager/user/show.html.twig', array(
            'user' => $user,
			'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="manager_user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user, ['password' => true]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();

			try
			{
				// $em = $this->getDoctrine()->getManager();
				
				// if ($user->getPasswordText()) {
				// 	$user->setPassword($this->encodePassword($user));
				// }
				
				// $em->persist($user);
				// $em->flush($user);

				if ($user->getPasswordText()) {
					$user->setPassword($this->encodePassword($user));
				}

				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Usuário gravado com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
			}            

            return $this->redirectToRoute('manager_user_edit', array('id' => $user->getId()));
        }

        return $this->render('manager/user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="manager_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();

			try
			{
				$em = $this->getDoctrine()->getManager();
				$em->remove($user);
				$em->flush();
				
				$session->getFlashBag()->add('success', 'Usuário removido com sucesso..');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
			}            
        }

        return $this->redirectToRoute('manager_user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Encodes the password.
     *
     * @param string Plain Text Password
     *
     * @return string Encoded Password
     */
    private function encodePassword($user) 
    {
        $encoder = $this->container->get('security.password_encoder');
        
        return $encoder->encodePassword($user, $user->getPasswordText());
    }  
}
