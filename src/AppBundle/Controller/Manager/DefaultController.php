<?php

namespace AppBundle\Controller\Manager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Default controller.
 *
 * @Route("manager")
 */
class DefaultController extends Controller
{
    /**
     * Manager welcome page.
     *
     * @Route("/", name="manager")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('manager/default/index.html.twig');
    }
}
