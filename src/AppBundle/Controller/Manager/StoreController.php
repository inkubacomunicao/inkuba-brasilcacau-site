<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\Store;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Store controller.
 *
 * @Route("manager/store")
 */
class StoreController extends Controller
{
    /**
     * Lists all store entities.
     *
     * @Route("/{filter}", name="manager_store_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.store');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			switch ($filter) {
				case 'active':
					$qb->andWhere('store.isActive = 1');
					break;
				case 'inactive':
					$qb->andWhere('store.isActive = 0');
					break;
			}
	
			return $responseService->getResponse();
		}

		return $this->render('manager/store/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new store entity.
     *
     * @Route("/new", name="manager_store_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $store = new Store();
        $form = $this->createForm('AppBundle\Form\StoreType', $store);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($store);
                $em->flush();

                $session->getFlashBag()->add('success', 'Loja gravada com sucesso.');

                return $this->redirectToRoute('manager_store_show', array('id' => $store->getId()));
            }
            catch(\Exception $ex)
            {
                $session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
            }
        }

        return $this->render('manager/store/new.html.twig', array(
            'store' => $store,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a store entity.
     *
     * @Route("/{id}", name="manager_store_show")
     * @Method("GET")
     */
    public function showAction(Store $store)
    {
        $deleteForm = $this->createDeleteForm($store);

        return $this->render('manager/store/show.html.twig', array(
            'store' => $store,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing store entity.
     *
     * @Route("/{id}/edit", name="manager_store_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Store $store)
    {
        $deleteForm = $this->createDeleteForm($store);
        $editForm = $this->createForm('AppBundle\Form\StoreType', $store);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('manager_store_edit', array('id' => $store->getId()));
        }

        return $this->render('manager/store/edit.html.twig', array(
            'store' => $store,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a store entity.
     *
     * @Route("/{id}", name="manager_store_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Store $store)
    {
        $form = $this->createDeleteForm($store);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($store);
            $em->flush();
        }

        return $this->redirectToRoute('manager_store_index');
    }

    /**
     * Creates a form to delete a store entity.
     *
     * @param Store $store The store entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Store $store)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_store_delete', array('id' => $store->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
