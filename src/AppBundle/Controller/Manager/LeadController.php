<?php

namespace AppBundle\Controller\Manager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Lead;
use AppBundle\Service\LeadMailNotifier;

/**
 * Lead controller.
 *
 * @Route("manager/lead")
 */
class LeadController extends Controller
{
    /**
     * Lists all lead entities.
     *
	 * @Route("/", name="manager_lead_index")
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request)
    {
		$datatable = $this->get('app.datatable.lead');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			return $responseService->getResponse();
		}

		return $this->render('manager/lead/index.html.twig', array(
			'datatable' => $datatable
        ));
    }

    /**
     * Finds and displays a lead entity.
     *
     * @Route("/{id}", name="manager_lead_show", requirements={"id": "[0-9A-Fa-f]{8}[-]?(?:[0-9A-Fa-f]{4}[-]?){3}[0-9A-Fa-f]{12}"})
     * @Method({"GET","POST"})
     */
    public function showAction(Lead $lead)
    {
        return $this->render('manager/lead/show.html.twig', array(
			'lead' => $lead,
        ));
	}
	
	/**
     * Generate download CSV.
     *
     * @Route("/download", name="manager_lead_download")
     * @Method("GET")
     */
    public function downloadAction(Request $request)
    {        
		$leads = $this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->findAll();
		$result = [];
		
		//TODO: Mark as read
		
		foreach ($leads as $i => $lead) {
			$result[$i]["Id"] = $lead->getId();
			$result[$i]["Campaign"] = $lead->getCampaign();
			$result[$i]["Nome"] = $lead->getName();
			$result[$i]["CPF"] = $lead->getCpf();
			$result[$i]["E-mail"] = $lead->getEmail();
			$result[$i]["ContatoPrimario"] = $lead->getPhonePrimary();
			$result[$i]["ContatoSecundario"] = $lead->getPhoneSecondary();

			$result[$i]["Endereco"] = $lead->getAddress();
			$result[$i]["Numero"] = $lead->getNumber();
			$result[$i]["Complemento"] = $lead->getComplement();
			$result[$i]["CEP"] = $lead->getCep();
			$result[$i]["Bairro"] = $lead->getNeighborhood();
			
			if ($lead->getCity()) {
				$result[$i]["Cidade"] = $lead->getCity()->getName();
				$result[$i]["Estado"] = $lead->getCity()->getState()->getAbbreviation();
			} else {
				$result[$i]["Cidade"] = '';
				$result[$i]["Estado"] = '';				
			}

			$result[$i]["Aceite Termos"] = $lead->getHasAgreed() ? 'Sim' : 'Não';
			$result[$i]["Received At"] = $lead->getCreatedAt()->format('Y-m-d H:i:s');
		}

		$csv = $this->container->get('serializer')->encode($result, 'csv');

		$response = new Response($csv, Response::HTTP_OK, ['content-type' => 'text/csv']);
		$disposition = $response->headers->makeDisposition('attachment', sprintf('leads_download_%s.csv', date('YmdHi')));
		$response->headers->set('Content-Disposition', $disposition);

		return $response;    
	}  	
}
