<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Service\ImageUploader;


/**
 * Product controller.
 *
 * @Route("manager/product")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/{filter}", name="manager_product_index", requirements={"filter": "(all|active|inactive|$)"})
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
        $imagePath = $this->container->getParameter('image_upload_folder');
        $datatable = $this->get('app.datatable.product');
        $datatable->buildDatatable(['imagePath' => $imagePath]);

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $qb = $datatableQueryBuilder->getQb();

            switch ($filter) {
                case 'active':
                    $qb->andWhere('product.isActive = 1');
                    break;
                case 'inactive':
                    $qb->andWhere('product.isActive = 0');
                    break;
            }

            return $responseService->getResponse();
        }

        return $this->render('manager/product/index.html.twig', array(
            'datatable' => $datatable,
            'filter' => $filter,
        ));
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="manager_product_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $product = new Product();

        $imagine = $this->get('liip_imagine.cache.manager');
		$imagePath = $this->container->getParameter('image_upload_folder');	  

        $form = $this->createForm('AppBundle\Form\ProductType', $product, ['imagine' => $imagine, 'imagePath' => $imagePath]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try
            {
                $uploader = $this->get(ImageUploader::class);

                $product->setMobileImage($uploader->save($product->getMobileImage()));
                $product->setDesktopImage($uploader->save($product->getDesktopImage()));
                $product->setIsActive(true);

                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();

                $session->getFlashBag()->add('success', 'Produto gravado com sucesso.');

                return $this->redirectToRoute('manager_product_show', array('id' => $product->getId()));
            }
            catch(\Exception $ex)
            {
                $session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
            }
        }

        return $this->render('manager/product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="manager_product_show")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        $uploader = $this->get(ImageUploader::class);

        $product->setMobileImage($uploader->retrieve($product->getMobileImage()));
        $product->setDesktopImage($uploader->retrieve($product->getDesktopImage()));

        $deleteForm = $this->createDeleteForm($product);

        return $this->render('manager/product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="manager_product_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Product $product)
    {
        $uploader = $this->get(ImageUploader::class);

        $product->setMobileImage($uploader->retrieve($product->getMobileImage()));
        $product->setDesktopImage($uploader->retrieve($product->getDesktopImage()));

        $deleteForm = $this->createDeleteForm($product);

		$imagine = $this->get('liip_imagine.cache.manager');
		$imagePath = $this->container->getParameter('image_upload_folder');	        
        
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product, ['requireUpload' => false,'imagine' => $imagine, 'imagePath' => $imagePath]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();

            try
            {
                if ($product->getMobileImage() != null && $mobileImage = $uploader->save($product->getMobileImage()))
                    $product->setMobileImage($mobileImage);

                if ($product->getDesktopImage() != null && $desktopImage = $uploader->save($product->getDesktopImage()))
                    $product->setDesktopImage($desktopImage);

                $this->getDoctrine()->getManager()->flush();
                $session->getFlashBag()->add('success', 'Produto gravado com sucesso.');

            }
            catch(\Exception $ex)
            {
                $session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
            }

            return $this->redirectToRoute('manager_product_edit', array('id' => $product->getId()));
        }

        return $this->render('manager/product/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="manager_product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('manager_product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}