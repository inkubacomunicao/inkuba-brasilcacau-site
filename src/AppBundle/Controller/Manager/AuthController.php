<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Authentication controller.
 *
 * @Route("manager/auth")
 */
class AuthController extends Controller
{
    /**
     * @Route("/login", name="manager_auth_login")
     */
    public function loginAction(Request $request)
    {
        $authentication = $this->get('security.authentication_utils');

        return $this->render('manager/auth/login.html.twig', [
            'last_login' => $authentication->getLastUsername(),
            'error' => $authentication->getLastAuthenticationError(),
        ]);
    }        

    /**
     * @Route("/logout", name="manager_auth_logout")
     */
    public function logoutAction(Request $request)
    {
        return $this->render('manager/auth/logout.html.twig', []);
    }           
}
