<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\State;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * State controller.
 *
 * @Route("manager/state")
 */
class StateController extends Controller
{
    /**
     * Lists all state entities.
     *
     * @Route("/{filter}", name="manager_state_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.state');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();
	
			return $responseService->getResponse();
		}

		return $this->render('manager/state/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Finds and displays a state entity.
     *
     * @Route("/{id}", name="manager_state_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, State $state)
    {
		$datatable = $this->get('app.datatable.city');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();
			$qb->andWhere('IDENTITY(city.state) = :id' );
			$qb->setParameter('id', $state->getId());

			return $responseService->getResponse();
		}

        return $this->render('manager/state/show.html.twig', array(
			'state' => $state,
			'datatable' => $datatable,
        ));
    }
}
