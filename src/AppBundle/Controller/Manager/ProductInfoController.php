<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\ProductInfo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * ProductInfo controller.
 *
 * @Route("manager/product-info")
 */
class ProductInfoController extends Controller
{
    /**
     * Lists all product_info entities.
     *
     * @Route("/{filter}", name="manager_product_info_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.product_info');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			switch ($filter) {
				case 'active':
					$qb->andWhere('product_info.isActive = 1');
					break;
				case 'inactive':
					$qb->andWhere('product_info.isActive = 0');
					break;
			}
	
			return $responseService->getResponse();
		}

		return $this->render('manager/product-info/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new product_info entity.
     *
     * @Route("/new", name="manager_product_info_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $product_info = new ProductInfo();
        $form = $this->createForm('AppBundle\Form\ProductInfoType', $product_info);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{
				$em = $this->getDoctrine()->getManager();
				$em->persist($product_info);
				$em->flush();

				$session->getFlashBag()->add('success', 'Informação de produto gravada com sucesso.');
				
				return $this->redirectToRoute('manager_product_info_show', array('id' => $product_info->getId()));
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}			
        }

        return $this->render('manager/product-info/new.html.twig', array(
            'product_info' => $product_info,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product_info entity.
     *
     * @Route("/{id}", name="manager_product_info_show")
     * @Method("GET")
     */
    public function showAction(ProductInfo $product_info)
    {
        $deleteForm = $this->createDeleteForm($product_info);

        return $this->render('manager/product-info/show.html.twig', array(
            'product_info' => $product_info,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product_info entity.
     *
     * @Route("/{id}/edit", name="manager_product_info_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ProductInfo $product_info)
    {
        $deleteForm = $this->createDeleteForm($product_info);
        $editForm = $this->createForm('AppBundle\Form\ProductInfoType', $product_info);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();
			
			try
			{
				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Informação de produto gravada com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}            

            return $this->redirectToRoute('manager_product_info_edit', array('id' => $product_info->getId()));			
        }

        return $this->render('manager/product-info/edit.html.twig', array(
            'product_info' => $product_info,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product_info entity.
     *
     * @Route("/{id}", name="manager_product_info_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ProductInfo $product_info)
    {
        $form = $this->createDeleteForm($product_info);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{			
				$em = $this->getDoctrine()->getManager();
				$em->remove($product_info);
				$em->flush();

				$session->getFlashBag()->add('success', 'Informação de produto removida com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao remover. ' . $ex->getMessage());
			}          				
        }

        return $this->redirectToRoute('manager_product_info_index');
    }

    /**
     * Creates a form to delete a product_info entity.
     *
     * @param ProductInfo $product_info The product_info entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductInfo $product_info)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_product_info_delete', array('id' => $product_info->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
