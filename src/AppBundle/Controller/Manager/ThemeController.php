<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\Theme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Service\ImageUploader;

/**
 * Theme controller.
 *
 * @Route("manager/theme")
 */
class ThemeController extends Controller
{
    /**
     * Lists all theme entities.
     *
     * @Route("/{filter}", name="manager_theme_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.theme');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			switch ($filter) {
				case 'active':
					$qb->andWhere('theme.isActive = 1');
					break;
				case 'inactive':
					$qb->andWhere('theme.isActive = 0');
					break;
			}
	
			return $responseService->getResponse();
		}

		return $this->render('manager/theme/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new theme entity.
     *
     * @Route("/new", name="manager_theme_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $theme = new Theme();
        $form = $this->createForm('AppBundle\Form\ThemeType', $theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{
                $em = $this->getDoctrine()->getManager();
                $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:Banner");
        
                $uploader = $this->get(ImageUploader::class);
        
                $theme->setPageBackgroundImage($uploader->save($theme->getPageBackgroundImage()));
                $theme->setTextBackgroundImage($uploader->save($theme->getTextBackgroundImage()));
                $theme->setTopLeftParallaxImage($uploader->save($theme->getTopLeftParallaxImage()));
                $theme->setBottomLeftParallaxImage($uploader->save($theme->getBottomLeftParallaxImage()));
                $theme->setTopRightParallaxImage($uploader->save($theme->getTopRightParallaxImage()));
                $theme->setBottomRightParallaxImage($uploader->save($theme->getBottomRightParallaxImage()));
                $theme->setTitleBackgroundImage($uploader->save($theme->getTitleBackgroundImage()));
                $theme->setIsActive(true);

				$em->persist($theme);
				$em->flush();

				$session->getFlashBag()->add('success', 'Tema gravado com sucesso.');
				
				return $this->redirectToRoute('manager_theme_show', array('id' => $theme->getId()));
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}			
        }

        return $this->render('manager/theme/new.html.twig', array(
            'theme' => $theme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a theme entity.
     *
     * @Route("/{id}", name="manager_theme_show")
     * @Method("GET")
     */
    public function showAction(Theme $theme, Request $request)
    {
        $deleteForm = $this->createDeleteForm($theme);

        $imagePath = $this->container->getParameter('image_upload_folder');
		$datatable = $this->get('app.datatable.product');
		$datatable->buildDatatable(['imagePath' => $imagePath]);
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();
			$qb->join('product.theme','c');
			$qb->andWhere('c.id = :id' );
			$qb->setParameter('id', $theme->getId());
	
			return $responseService->getResponse();
		}

        return $this->render('manager/theme/show.html.twig', array(
            'theme' => $theme,
			'delete_form' => $deleteForm->createView(),
			'datatable' => $datatable,
        ));
    }

    /**
     * Displays a form to edit an existing theme entity.
     *
     * @Route("/{id}/edit", name="manager_theme_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Theme $theme)
    {
        $uploader = $this->get(ImageUploader::class);

        $theme->setPageBackgroundImage($uploader->retrieve($theme->getPageBackgroundImage()));
        $theme->setTextBackgroundImage($uploader->retrieve($theme->getTextBackgroundImage()));
        $theme->setTopLeftParallaxImage($uploader->retrieve($theme->getTopLeftParallaxImage()));
        $theme->setBottomLeftParallaxImage($uploader->retrieve($theme->getBottomLeftParallaxImage()));
        $theme->setTopRightParallaxImage($uploader->retrieve($theme->getTopRightParallaxImage()));
        $theme->setBottomRightParallaxImage($uploader->retrieve($theme->getBottomRightParallaxImage()));
        $theme->setTitleBackgroundImage($uploader->retrieve($theme->getTitleBackgroundImage()));

        $editForm = $this->createForm('AppBundle\Form\ThemeType', $theme);
        $editForm->handleRequest($request);
        $deleteForm = $this->createDeleteForm($theme);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();
			
			try
			{
                if ($theme->getPageBackgroundImage() != null && $pageBackgroundImage = $uploader->save($theme->getPageBackgroundImage()))
                    $theme->setPageBackgroundImage($pageBackgroundImage);
    
                if ($theme->getTextBackgroundImage() != null && $textBackgroundImage = $uploader->save($theme->getTextBackgroundImage()))
                    $theme->setTextBackgroundImage($textBackgroundImage);    
        
                if ($theme->getTopLeftParallaxImage() != null && $topLeftParallaxImage = $uploader->save($theme->getTopLeftParallaxImage()))
                    $theme->setTopLeftParallaxImage($topLeftParallaxImage);
                
                if ($theme->getBottomLeftParallaxImage() != null && $bottomLeftParallaxImage = $uploader->save($theme->getBottomLeftParallaxImage()))
                    $theme->setBottomLeftParallaxImage($bottomLeftParallaxImage);
                    
                if ($theme->getTopRightParallaxImage() != null && $topRightParallaxImage = $uploader->save($theme->getTopRightParallaxImage()))
                    $theme->setTopRightParallaxImage($topRightParallaxImage);
                    
                if ($theme->getBottomRightParallaxImage() != null && $bottomRightParallaxImage = $uploader->save($theme->getBottomRightParallaxImage()))
                    $theme->setBottomRightParallaxImage($bottomRightParallaxImage);
                    
                if ($theme->getTitleBackgroundImage() != null && $titleBackgroundImage = $uploader->save($theme->getTitleBackgroundImage()))
                    $theme->setTitleBackgroundImage($titleBackgroundImage);    

				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Tema gravado com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}            

            return $this->redirectToRoute('manager_theme_edit', array('id' => $theme->getId()));			
        }

        return $this->render('manager/theme/edit.html.twig', array(
            'theme' => $theme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a theme entity.
     *
     * @Route("/{id}", name="manager_theme_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Theme $theme)
    {
        $form = $this->createDeleteForm($theme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{			
				$em = $this->getDoctrine()->getManager();
				$em->remove($theme);
				$em->flush();

				$session->getFlashBag()->add('success', 'Tema removido com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao remover. ' . $ex->getMessage());
			}          				
        }

        return $this->redirectToRoute('manager_theme_index');
    }

    /**
     * Creates a form to delete a theme entity.
     *
     * @param Theme $theme The theme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Theme $theme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_theme_delete', array('id' => $theme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
