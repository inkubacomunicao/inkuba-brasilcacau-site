<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\Banner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Service\ImageUploader;

/**
 * Banner controller.
 *
 * @Route("manager/banner")
 */
class BannerController extends Controller
{
    /**
     * Lists all banner entities.
     *
     * @Route("/{filter}", name="manager_banner_index", requirements={"filter": "(all|active|inactive|$)"})
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
        $imagePath = $this->container->getParameter('image_upload_folder');

        $datatable = $this->get('app.datatable.banner');
        $datatable->buildDatatable(['imagePath' => $imagePath]);

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $qb = $datatableQueryBuilder->getQb();

            switch ($filter) {
                case 'active':
                    $qb->andWhere('banner.isActive = 1');
                    break;
                case 'inactive':
                    $qb->andWhere('banner.isActive = 0');
                    break;
            }

            return $responseService->getResponse();
        }

        return $this->render('manager/banner/index.html.twig', array(
            'datatable' => $datatable,
            'filter' => $filter,
        ));
    }

    /**
     * Creates a new banner entity.
     *
     * @Route("/new", name="manager_banner_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $banner = new Banner();
        $form = $this->createForm('AppBundle\Form\BannerType', $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try
            {
                $em = $this->getDoctrine()->getManager();
                $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:Banner");

                $uploader = $this->get(ImageUploader::class);

                $banner->setMobileImage($uploader->save($banner->getMobileImage()));
                $banner->setDesktopImage($uploader->save($banner->getDesktopImage()));
                $banner->setOrdenator($repo->findMaxOrdenator() + 1);

                $em->persist($banner);
                $em->flush();

                $session->getFlashBag()->add('success', 'Banner gravado com sucesso.');

                return $this->redirectToRoute('manager_banner_show', array('id' => $banner->getId()));
            }
            catch(\Exception $ex)
            {
                $session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
            }
        }

        return $this->render('manager/banner/new.html.twig', array(
            'banner' => $banner,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a banner entity.
     *
     * @Route("/{id}", name="manager_banner_show")
     * @Method("GET")
     */
    public function showAction(Banner $banner)
    {
        $mobile = $this->container->getParameter('image_upload_directory') . DIRECTORY_SEPARATOR . $banner->getMobileImage();
        $desktop = $this->container->getParameter('image_upload_directory') . DIRECTORY_SEPARATOR . $banner->getDesktopImage();

        $banner->setMobileImage(new File($mobile));
        $banner->setDesktopImage(new File($desktop));

        $deleteForm = $this->createDeleteForm($banner);

        return $this->render('manager/banner/show.html.twig', array(
            'banner' => $banner,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing banner entity.
     *
     * @Route("/{id}/edit", name="manager_banner_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Banner $banner)
    {
        $uploader = $this->get(ImageUploader::class);

        $banner->setMobileImage($uploader->retrieve($banner->getMobileImage()));
        $banner->setDesktopImage($uploader->retrieve($banner->getDesktopImage()));

        $editForm = $this->createForm('AppBundle\Form\BannerType', $banner, ['requireUpload' => false]);
        $editForm->handleRequest($request);

        $deleteForm = $this->createDeleteForm($banner);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();
			
			try
			{
                if ($banner->getMobileImage() != null && $mobileImage = $uploader->save($banner->getMobileImage()))
                    $banner->setMobileImage($mobileImage);

                if ($banner->getDesktopImage() != null && $desktopImage = $uploader->save($banner->getDesktopImage()))
                    $banner->setDesktopImage($desktopImage);

				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Banner gravado com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}            

            return $this->redirectToRoute('manager_banner_edit', array('id' => $banner->getId()));
        }

        return $this->render('manager/banner/edit.html.twig', array(
            'banner' => $banner,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a banner entity.
     *
     * @Route("/{id}", name="manager_banner_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Banner $banner)
    {
        $form = $this->createDeleteForm($banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($banner);
                $em->flush();

                $session->getFlashBag()->add('success', 'Banner removido com sucesso.');
            }
            catch(\Exception $ex)
            {
                $session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
            }
        }

        return $this->redirectToRoute('manager_banner_index');
    }


    /**
     * Creates a form to delete a banner entity.
     *
     * @param Banner $banner The banner entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Banner $banner)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_banner_delete', array('id' => $banner->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
