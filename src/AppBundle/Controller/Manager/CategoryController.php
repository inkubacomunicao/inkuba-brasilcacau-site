<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Service\ImageUploader;

/**
 * Category controller.
 *
 * @Route("manager/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/{filter}", name="manager_category_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.category');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			switch ($filter) {
				case 'active':
					$qb->andWhere('category.isActive = 1');
					break;
				case 'inactive':
					$qb->andWhere('category.isActive = 0');
					break;
			}
	
			return $responseService->getResponse();
		}

		return $this->render('manager/category/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/new", name="manager_category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('AppBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{
                $em = $this->getDoctrine()->getManager();
                $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:Banner");

                $uploader = $this->get(ImageUploader::class);

                $category->setPageBackgroundImage($uploader->save($category->getPageBackgroundImage()));
                $category->setTextBackgroundImage($uploader->save($category->getTextBackgroundImage()));
                $category->setTopLeftParallaxImage($uploader->save($category->getTopLeftParallaxImage()));
                $category->setBottomLeftParallaxImage($uploader->save($category->getBottomLeftParallaxImage()));
                $category->setTopRightParallaxImage($uploader->save($category->getTopRightParallaxImage()));
                $category->setBottomRightParallaxImage($uploader->save($category->getBottomRightParallaxImage()));
                $category->setTitleBackgroundImage($uploader->save($category->getTitleBackgroundImage()));
                $category->setIsActive(true);
            
				$em->persist($category);
				$em->flush();

				$session->getFlashBag()->add('success', 'Categoria gravada com sucesso.');
				
				return $this->redirectToRoute('manager_category_show', array('id' => $category->getId()));
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}			
        }

        return $this->render('manager/category/new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="manager_category_show")
     * @Method("GET")
     */
    public function showAction(Category $category, Request $request)
    {
        $deleteForm = $this->createDeleteForm($category);

        $imagePath = $this->container->getParameter('image_upload_folder');
		$datatable = $this->get('app.datatable.product');
		$datatable->buildDatatable(['imagePath' => $imagePath]);
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();
			$qb->join('product.categories','c');
			$qb->andWhere('c.id = :id' );
			$qb->setParameter('id', $category->getId());
	
			return $responseService->getResponse();
		}

        return $this->render('manager/category/show.html.twig', array(
            'category' => $category,
			'delete_form' => $deleteForm->createView(),
			'datatable' => $datatable,
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="manager_category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category)
    {
        $uploader = $this->get(ImageUploader::class);

        $category->setPageBackgroundImage($uploader->retrieve($category->getPageBackgroundImage()));
        $category->setTextBackgroundImage($uploader->retrieve($category->getTextBackgroundImage()));
        $category->setTopLeftParallaxImage($uploader->retrieve($category->getTopLeftParallaxImage()));
        $category->setBottomLeftParallaxImage($uploader->retrieve($category->getBottomLeftParallaxImage()));
        $category->setTopRightParallaxImage($uploader->retrieve($category->getTopRightParallaxImage()));
        $category->setBottomRightParallaxImage($uploader->retrieve($category->getBottomRightParallaxImage()));
        $category->setTitleBackgroundImage($uploader->retrieve($category->getTitleBackgroundImage()));

        $editForm = $this->createForm('AppBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);
        $deleteForm = $this->createDeleteForm($category);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();
			
			try
			{
                if ($category->getPageBackgroundImage() != null && $pageBackgroundImage = $uploader->save($category->getPageBackgroundImage()))
                    $category->setPageBackgroundImage($pageBackgroundImage);
    
                if ($category->getTextBackgroundImage() != null && $textBackgroundImage = $uploader->save($category->getTextBackgroundImage()))
                    $category->setTextBackgroundImage($textBackgroundImage);    
        
                if ($category->getTopLeftParallaxImage() != null && $topLeftParallaxImage = $uploader->save($category->getTopLeftParallaxImage()))
                    $category->setTopLeftParallaxImage($topLeftParallaxImage);
                
                if ($category->getBottomLeftParallaxImage() != null && $bottomLeftParallaxImage = $uploader->save($category->getBottomLeftParallaxImage()))
                    $category->setBottomLeftParallaxImage($bottomLeftParallaxImage);
                    
                if ($category->getTopRightParallaxImage() != null && $topRightParallaxImage = $uploader->save($category->getTopRightParallaxImage()))
                    $category->setTopRightParallaxImage($topRightParallaxImage);
                    
                if ($category->getBottomRightParallaxImage() != null && $bottomRightParallaxImage = $uploader->save($category->getBottomRightParallaxImage()))
                    $category->setBottomRightParallaxImage($bottomRightParallaxImage);
                    
                if ($category->getTitleBackgroundImage() != null && $titleBackgroundImage = $uploader->save($category->getTitleBackgroundImage()))
                    $category->setTitleBackgroundImage($titleBackgroundImage);    

				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Categoria gravada com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar ' . $ex->getMessage());
			}            

            return $this->redirectToRoute('manager_category_edit', array('id' => $category->getId()));			
        }

        return $this->render('manager/category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="manager_category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();
			
			try
			{			
				$em = $this->getDoctrine()->getManager();
				$em->remove($category);
				$em->flush();

				$session->getFlashBag()->add('success', 'Categoria removida com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao remover. ' . $ex->getMessage());
			}          				
        }

        return $this->redirectToRoute('manager_category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
