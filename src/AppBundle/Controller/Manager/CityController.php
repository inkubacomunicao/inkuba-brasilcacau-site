<?php

namespace AppBundle\Controller\Manager;

use AppBundle\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * City controller.
 *
 * @Route("manager/city")
 */
class CityController extends Controller
{
    /**
     * Lists all city entities.
     *
     * @Route("/{filter}", name="manager_city_index", requirements={"filter": "(all|active|inactive|$)"})
	 * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, $filter="")
    {
		$datatable = $this->get('app.datatable.city');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();

			return $responseService->getResponse();
		}

		return $this->render('manager/city/index.html.twig', array(
			'datatable' => $datatable,
			'filter' => $filter,
        ));
    }

    /**
     * Creates a new city entity.
     *
     * @Route("/new", name="manager_city_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $city = new City();
        $form = $this->createForm('AppBundle\Form\CityType', $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();

			try 
			{
				$em = $this->getDoctrine()->getManager();
				$em->persist($city);
				$em->flush();
	
				$session->getFlashBag()->add('success', 'Cidade gravada com sucesso.');

				return $this->redirectToRoute('manager_city_show', array('id' => $city->getId()));
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao gravar. ' . $ex->getMessage());
			}
        }

        return $this->render('manager/city/new.html.twig', array(
            'city' => $city,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a city entity.
     *
     * @Route("/{id}", name="manager_city_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, City $city)
    {
        $deleteForm = $this->createDeleteForm($city);

		$datatable = $this->get('app.datatable.property');
		$datatable->buildDatatable();
	
		if ($request->isXmlHttpRequest()) {
			$responseService = $this->get('sg_datatables.response');
			$responseService->setDatatable($datatable);
	
			$datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
			$qb = $datatableQueryBuilder->getQb();
			$qb->andWhere('IDENTITY(property.city) = :id' );
			$qb->setParameter('id', $city->getId());
	
			return $responseService->getResponse();
		}

        return $this->render('manager/city/show.html.twig', array(
            'city' => $city,
			'delete_form' => $deleteForm->createView(),
			'datatable' => $datatable,
        ));
    }

    /**
     * Displays a form to edit an existing city entity.
     *
     * @Route("/{id}/edit", name="manager_city_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, City $city)
    {
        $deleteForm = $this->createDeleteForm($city);
        $editForm = $this->createForm('AppBundle\Form\CityType', $city);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$session = new Session();

			try
			{
				$this->getDoctrine()->getManager()->flush();
				$session->getFlashBag()->add('success', 'Cidade salva com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Error saving. ' . $ex->getMessage());				
			}            

            return $this->redirectToRoute('manager_city_edit', array('id' => $city->getId()));
        }

        return $this->render('manager/city/edit.html.twig', array(
            'city' => $city,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a city entity.
     *
     * @Route("/{id}", name="manager_city_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, City $city)
    {
        $form = $this->createDeleteForm($city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$session = new Session();

			try
			{
				$em = $this->getDoctrine()->getManager();
				$em->remove($city);
				$em->flush();
				
				$session->getFlashBag()->add('success', 'Cidade removida com sucesso.');
			}
			catch(\Exception $ex)
			{
				$session->getFlashBag()->add('error', 'Erro ao salvar. ' . $ex->getMessage());
			}            
        }

        return $this->redirectToRoute('manager_city_index');
    }

    /**
     * Creates a form to delete a city entity.
     *
     * @param City $city The city entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(City $city)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manager_city_delete', array('id' => $city->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
