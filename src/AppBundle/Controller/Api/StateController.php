<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Context\Context;

/**
 * State controller.
 *
 * @Route("api/")
 */
class StateController extends FOSRestController
{
    /**
     * @Get("states/", name="api_state_list", defaults={ "_format" = "json" })
     */
    public function getStates(Request $request)
    {
        $states = $this->getDoctrine()->getManager()->getRepository("AppBundle:State")->findBy([], ['name' => 'ASC']);

        if ($states) {
            $context = new Context();
            $context->addGroup('state');

            $view = $this->view(['code' => 200, 'message' => 'Success.', 'states' => $states], 200)->setContext($context);
        } else {
            throw new HttpException(404, 'No records.');
        }

        return $this->handleView($view);
    }

    /**
     * @Get("states/{abbreviation}/cities/", name="api_state_cities_list", defaults={ "_format" = "json" })
     */
    public function getStateCities(Request $request, $abbreviation)
    {
        $cities = $this->getDoctrine()->getManager()->getRepository("AppBundle:City")
            ->createQueryBuilder('c')
            ->join('c.state', 's')
            ->where('s.abbreviation = :abbreviation')
            ->setParameter('abbreviation', $abbreviation)
            ->orderBy('c.name')
            ->getQuery()->getResult();

        if ($cities) {
            $context = new Context();
            $context->addGroup('state_and_cities');

            $view = $this->view(['code' => 200, 'message' => 'Success.', 'cities' => $cities], 200)->setContext($context);
        } else {
            throw new HttpException(404, 'No records.');
        }

        return $this->handleView($view);
    }
}