<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Context\Context;

/**
 * State controller.
 *
 * @Route("api/")
 */
class StoreController extends FOSRestController
{
    /**
     * @Get("stores/", name="api_store_list", defaults={ "_format" = "json" })
     */
    public function getStores(Request $request)
    {
        $stores = $this->getDoctrine()->getManager()->getRepository("AppBundle:Store")->findBy(['isActive' => true], ['name' => 'ASC']);

        if ($stores) {
            $context = new Context();
            $context->addGroup('store');

            $view = $this->view(['code' => 200, 'message' => 'Success', 'stores' => $stores], 200)->setContext($context);
        } else {
            throw new HttpException(404, 'No records.');
        }

        return $this->handleView($view);
    }
}