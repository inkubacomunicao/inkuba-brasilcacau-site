<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Context\Context;

/**
 * City controller.
 *
 * @Route("api/")
 */
class CityController extends FOSRestController
{
   /**
    * @Get("cities/", name="api_cities", defaults={ "_format" = "json" })
    */    
    public function getCities(Request $request)
    {
		try 
		{
			$cities = $this->getDoctrine()->getManager()->getRepository("AppBundle:City")->findBy([], ['name' => 'ASC']);
			
			if ($cities) {
				$context = new Context();
				$context->addGroup('city');
	
				$view = $this->view(['code' => 200, 'message' => 'Success', 'cities' => $cities], 200)->setContext($context);
			} 
			else 
			{
				$view = $this->view(['code' => 404, 'message' => 'Not Found'], 404);
			}
		} 
		catch (\Exception $ex)
		{
			$view = $this->view(['code' => 500, 'message' => 'Internal Server Error'], 500);
		}

        return $this->handleView($view);        
	}  	
}