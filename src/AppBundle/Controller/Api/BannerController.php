<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Context\Context;
use AppBundle\Entity\Banner;


/**
 * Banner controller.
 *
 * @Route("api/banner")
 */
class BannerController extends FOSRestController
{
   /**
    * @Post("/{id}/ordenator/{direction}", name="api_banner_ordenator", defaults={ "_format" = "json" }, requirements={ "id": "\d+", "direction": "up|down" })
    */   
    public function postBannerOrdenator(Banner $banner, $direction) {
		try 
		{
			$em = $this->getDoctrine()->getManager();
            $repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:Banner");
            
            $previousOrdenator = $banner->getOrdenator();
            $newOrdenator = $banner->getOrdenator();

            switch ($direction) {
                case 'up':
                    $newOrdenator = $repo->findNewOrdenator($banner->getId(), 1);
                    break;
                case 'down':
                    $newOrdenator = $repo->findNewOrdenator($banner->getId(), -1);
                    break;
                default:
                    throw new Exception("Invalid argument for direction.", 1); //Nunca irá acontecer caso a opção required esteja correta na rota.
                    break;
            }

            $target = $repo->findOneByOrdenator($newOrdenator);
            
            $target->setOrdenator($previousOrdenator);
            $banner->setOrdenator($newOrdenator);            
            
            $em->persist($target);
            $em->persist($banner);

			$em->flush();
			
			$view = $this->view($banner, 201);
		} 
		catch (\Exception $ex)
		{
			$view = $this->view(['code' => 500, 'message' => 'Internal Server Error', 'exception' => $ex->getMessage()], 500);
		}

        return $this->handleView($view); 
    }

   /**
    * @Post("/{id}/isactive/{status}", name="api_banner_active", defaults={ "_format" = "json" }, requirements={ "id": "\d+", "status": "1|0" })
    */   
    public function postBannerActive(Banner $banner, $status) {
		try 
		{
			$em = $this->getDoctrine()->getManager();

			$context = new Context();	
			
			$banner->setIsActive($status);
			
			$em->persist($banner);
			$em->flush();
			
			$view = $this->view($banner, 201);
		} 
		catch (\Exception $ex)
		{
			$view = $this->view(['code' => 500, 'message' => 'Internal Server Error', 'exception' => $ex->getMessage()], 500);
		}

        return $this->handleView($view); 
    }    
}