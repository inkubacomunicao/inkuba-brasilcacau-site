<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AssertCustom;

/**
 * Contact
 */
class Contact
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Digite seu primeiro nome."
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Digite seu sobrenome."
     * )
     */
    private $surname;    

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Escolha o assunto."
     * )
     */
    private $subject;    

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Preencha com uma mensagem."
     * )
     */
    private $message;    

    /**
     * @Assert\NotNull()
     */
    private $city;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Preencha com seu e-mail de contato."
     * )
     * @Assert\Email(
     *      message = "O e-mail deve ser válido.",
     *      checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *      message = "Preencha com seu telefone de contato."
     * )
     * @Assert\Regex(
     *      pattern="/\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$/",
     *      match=true,
     *      message="Preencha com um telefone válido."
     * )
     */
    private $phone;

    /**
     * @var string
     *
     * @Assert\Regex(
     *      pattern="/^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$/",
     *      match=true,
     *      message="Preencha com um telefone válido."
     * )
     */
    private $mobile;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Contact
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }    

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }    

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Contact
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobileForSMS()
    {
        if ($this->mobile) {
            preg_match('/\(([\d]{2})\)\s([\d]{4,5})\-([\d]{4})/', $this->mobile, $matches);

            if (count($matches) == 4) {
                return sprintf('55 %s %s%s', $matches[1], $matches[2], $matches[3]);
            }    
        } 
        
        return null;
    }    

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Contact
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }    
}
