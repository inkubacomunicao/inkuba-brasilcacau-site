<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 *
 * @ORM\Table(
 * 		name="products",
 * 		indexes={    
 *      	@ORM\Index(name="productses_slug_idx", columns={"slug"})
 *      })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @UniqueEntity(
 *     fields={"slug"},
 *     errorPath="slug",
 *     message="Este slug já está sendo usado."
 * ) 
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
	 * @Assert\NotNull()
     */
	private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="desktop_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=150, maxWidth=2560, minHeight=150, maxHeight=1200) 
     */
    private $desktopImage;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=100, maxWidth=2560, minHeight=100, maxHeight=1200)      
     */
    private $mobileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="net_weight", type="text", length=100, nullable=true)
     */
    private $netWeight;

    /**
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(name="related_products",
     *     joinColumns={@ORM\JoinColumn(name="product_a_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_b_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $relatedProducts;

	/**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
	 * @Assert\Regex(
     *     pattern="/[a-z0-9\-]+/",
     *     match=true,
     * ) 
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="products")
     * @ORM\JoinTable(name="products__categories",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="products")
     * @ORM\JoinColumn(name="theme_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $theme;

    /**
     * @ORM\ManyToMany(targetEntity="ProductInfo", inversedBy="products", fetch="EAGER")
     * @ORM\JoinTable(name="products__product_infos",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_info_id", referencedColumnName="id")}
     *      )
     */
    private $productInfos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    /**
     * Constructor
     */
    public function __construct()
    {
		$this->createdAt = new \Datetime();
		$this->productInfos = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set net weight
     *
     * @param string $netWeight
     *
     * @return Product
     */
    public function setNetWeight($netWeight)
    {
        $this->netWeight = $netWeight;

        return $this;
    }

    /**
     * Get net weight
     *
     * @return string
     */
    public function getNetWeight()
    {
        return $this->netWeight;
    }

   /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Product
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set categories
     *
     * @return Product
     */
    public function setCategories(ArrayCollection $categories = null)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Add category
     *
     * @return Product
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
        $category->addProduct($this);
        return $this;
    }

    /**
     * Get recommended products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelatedProducts()
    {
        return $this->relatedProducts;
    }

    /**
     * Set recommended products
     *
     * @return Product
     */
    public function setRelatedProducts(ArrayCollection $relatedProducts = null)
    {
        $this->relatedProducts = $relatedProducts;

        return $this;
    }


    /**
     * Get product infos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductInfos()
    {
        return $this->productInfos;
    }

    /**
     * Set product infos
     *
     * @return Product
     */
    public function setProductInfos(ArrayCollection $productInfos = null)
    {
        $this->productInfos = $productInfos;
        return $this;
    }

    /**
     * Add product info
     *
     * @return Product
     */
    public function addProductInfo(ProductInfo $productInfo)
    {
        $this->productInfos[] = $productInfo;
        return $this;
    }

    /**
     * Get theme
     *
     * @return Theme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set theme
     *
     * @param \AppBundle\Entity\Theme $theme
     *
     * @return Product
     */
    public function setTheme(Theme $theme = null)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Set desktopImage
     *
     * @param string $desktopImage
     *
     * @return Banner
     */
    public function setDesktopImage($desktopImage)
    {
        $this->desktopImage = $desktopImage;

        return $this;
    }

    /**
     * Get desktopImage
     *
     * @return string
     */
    public function getDesktopImage()
    {
        return $this->desktopImage;
    }

    /**
     * Set mobileImage
     *
     * @param string $mobileImage
     *
     * @return Banner
     */
    public function setMobileImage($mobileImage)
    {
        $this->mobileImage = $mobileImage;

        return $this;
    }

    /**
     * Get mobileImage
     *
     * @return string
     */
    public function getMobileImage()
    {
        return $this->mobileImage;
    }
}
