<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Ingredient
 *
 * @ORM\Table(name="product_info")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductInfoRepository")
 */
class ProductInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;


    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="productInfos", fetch="EAGER")
     */
    private $products;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredients", type="text", length=65535)
     */
    private $ingredients;

    /**
     * @var string
     *
     * @ORM\Column(name="nutrition_facts", type="text", length=65535)
     */
    private $nutritionFacts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProductInfo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ingredients
     *
     * @param string $ingredients
     *
     * @return ProductInfo
     */
    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * Get ingredients
     *
     * @return string
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * Set nutrition facts
     *
     * @param string $nutritionfacts
     *
     * @return ProductInfo
     */
    public function setNutritionFacts($nutritionFacts)
    {
        $this->nutritionFacts = $nutritionFacts;

        return $this;
    }

    /**
     * Get nutrition facts
     *
     * @return string
     */
    public function getNutritionFacts()
    {
        return $this->nutritionFacts;
    }

    /**
     * Get products
     *
     * @return Product
     */
    public function getProducts()
    {
        return $this->products;
    }


    /**
     * Set product
     *
     * @return ProductInfo
     */
    public function setProducts($products)
    {
        return $this->products = $products;

        return $this;
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return ProductInfo
     */
    public function addProduct(Product $product = null)
    {
        $this->products[] = $product;
        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Product
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}

