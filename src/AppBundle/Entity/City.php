<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * City
 *
 * @ORM\Table(name="cities",
 *      indexes={
 *          @ORM\Index(name="cities_slug_idx", columns={"slug"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository") 
 * @UniqueEntity(
 *     fields={"name", "state"},
 *     errorPath="name",
 *     message="There already is a city with that name for this state."
 * )
 * @UniqueEntity(
 *     fields={"slug"},
 *     errorPath="slug",
 *     message="This slug in already in use."
 * ) 
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
	 * @Groups({"city", "state_and_cities"})  
     */
    private $id;

	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
	 * @Groups({"city", "state_and_cities", "store"})
	 * @Assert\NotNull()
     */
	private $name;
	
	/**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
	 * @Groups({"city", "state_and_cities"}) 
	 * @Assert\NotNull()
	 * @Assert\Regex(
     *     pattern="/[a-z0-9\-]+/",
     *     match=true,
     * )  
     */
    private $slug;	

    /**
     * @ORM\OneToMany(targetEntity="Store", mappedBy="city")	  
     */
	private $stores;

    /**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="cities", fetch="EAGER")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
	 * @Groups({"city", "store"})
	 * @Assert\NotNull()
     */
	private $state;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get stores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStores()
    {
        return $this->stores;
    }
    
    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     *
     * @return City
     */
    public function setState(State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }    

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return City
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
