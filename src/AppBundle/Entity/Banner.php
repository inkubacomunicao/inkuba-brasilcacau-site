<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File as File;

/**
 * Banner
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BannerRepository")
 */
class Banner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="desktop_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=400, maxWidth=2560, minHeight=400, maxHeight=1200) 
     */
    private $desktopImage;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=200, maxWidth=2560, minHeight=200, maxHeight=1200)
     */
    private $mobileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=6000, nullable=true)
     * @Assert\Url()
     */
    private $url;    

    /**
     * @var int
     *
     * @ORM\Column(name="ordenator", type="integer")
     */
    private $ordenator;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_at", type="datetime", nullable=false)
     */
    private $publishAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
	 * @Assert\Expression(
     *     "this.getExpiresAt() == null || this.getPublishAt() < this.getExpiresAt()",
     *     message="Data de expiração deve ser maior que a data de publicação."
     * )      
     */
    private $expiresAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publishAt = new \Datetime();
		$this->expiresAt = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @return Banner
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set desktopImage
     *
     * @param string $desktopImage
     *
     * @return Banner
     */
    public function setDesktopImage($desktopImage)
    {
        $this->desktopImage = $desktopImage;

        return $this;
    }

    /**
     * Get desktopImage
     *
     * @return string
     */
    public function getDesktopImage()
    {
        return $this->desktopImage;
    }

    /**
     * Set mobileImage
     *
     * @param string $mobileImage
     *
     * @return Banner
     */
    public function setMobileImage($mobileImage)
    {
        $this->mobileImage = $mobileImage;

        return $this;
    }

    /**
     * Get mobileImage
     *
     * @return string
     */
    public function getMobileImage()
    {
        return $this->mobileImage;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Banner
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }    

    /**
     * Get ordenator
     *
     * @return int
     */
    public function getOrdenator()
    {
        return $this->ordenator;
    }

    /**
     * Set ordenator
     *
     * @return Banner
     */
    public function setOrdenator($ordenator)
    {
        $this->ordenator = $ordenator;

        return $this;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     *
     * @return Banner
     */
    public function setExpiresAt($expiresAt)
    {
		if ($expiresAt instanceof \DateTime) 
		{
			$this->expiresAt = $expiresAt;
		}
		else
		{
            if ($expiresAt != null) 
            {
                $this->expiresAt = new \DateTime($expiresAt);
            }
		}

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get the value of publishAt
     *
     * @return  \DateTime
     */ 
    public function getPublishAt()
    {
        return $this->publishAt;
    }

    /**
     * Set the value of publishAt
     *
     * @param  \DateTime  $publishAt
     *
     * @return  self
     */ 
    public function setPublishAt(\DateTime $publishAt)
    {
        if ($publishAt instanceof \DateTime) 
		{
			$this->publishAt = $publishAt;
		}
		else
		{
            $this->publishAt = new \DateTime($publishAt);
        }

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }    
}
