<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Store
 *
 * @ORM\Table(name="stores")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StoreRepository")
 */
class Store
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"store"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     * @Groups({"store"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Groups({"store"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address_complement", type="string", length=255, nullable=true)
     * @Groups({"store"})
     */
    private $addressComplement;

    /**
     * @var string
     *
     * @ORM\Column(name="address_district", type="string", length=255)
     * @Groups({"store"})
     */
    private $addressDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="address_zipcode", type="string", length=255)
     * @Groups({"store"})
     */
    private $addressZipcode;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="stores", fetch="EAGER")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * @Groups({"store"})
     * @Assert\NotNull()
     */
    private $city;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_coffee", type="boolean")
     * @Groups({"store"})
     */
    private $hasCoffee;

    /**
     * @var int
     *
     * @ORM\Column(name="phone_number", type="string", length=16, nullable=true)
     * @Groups({"store"})
     */
    private $phoneNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="whatsapp_number", type="string", length=16, nullable=true)
     * @Groups({"store"})
     */
    private $whatsappNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=6, nullable=true)
     * @Groups({"store"})
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=6, nullable=true)
     * @Groups({"store"})
     */
    private $longitude;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     * @Groups({"store"})
     */
    private $isActive;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Store
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Store
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Set addressComplement
     *
     * @param string $addressComplement
     *
     * @return Store
     */
    public function setAddressComplement($addressComplement)
    {
        $this->addressComplement = $addressComplement;

        return $this;
    }

    /**
     * Get addressComplement
     *
     * @return string
     */
    public function getAddressComplement()
    {
        return $this->addressComplement;
    }

    /**
     * Set addressDistrict
     *
     * @param string $addressDistrict
     *
     * @return Store
     */
    public function setAddressDistrict($addressDistrict)
    {
        $this->addressDistrict = $addressDistrict;

        return $this;
    }

    /**
     * Get addressDistrict
     *
     * @return string
     */
    public function getAddressDistrict()
    {
        return $this->addressDistrict;
    }

    /**
     * Set addressZipcode
     *
     * @param string $addressZipcode
     *
     * @return Store
     */
    public function setAddressZipcode($addressZipcode)
    {
        $this->addressZipcode = $addressZipcode;

        return $this;
    }

    /**
     * Get addressZipcode
     *
     * @return string
     */
    public function getAddressZipcode()
    {
        return $this->addressZipcode;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Property
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set hasCoffee
     *
     * @param boolean $hasCoffee
     *
     * @return Store
     */
    public function setHasCoffee($hasCoffee)
    {
        $this->hasCoffee = $hasCoffee;

        return $this;
    }

    /**
     * Get hasCoffee
     *
     * @return bool
     */
    public function getHasCoffee()
    {
        return $this->hasCoffee;
    }

    /**
     * Set phoneNumber
     *
     * @param integer $phoneNumber
     *
     * @return Store
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set whatsappNumber
     *
     * @param integer $whatsappNumber
     *
     * @return Store
     */
    public function setWhatsappNumber($whatsappNumber)
    {
        $this->whatsappNumber = $whatsappNumber;

        return $this;
    }

    /**
     * Get whatsappNumber
     *
     * @return int
     */
    public function getWhatsappNumber()
    {
        return $this->whatsappNumber;
    }    

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Store
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Store
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Store
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}

