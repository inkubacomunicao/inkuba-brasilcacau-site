<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * State
 *
 * @ORM\Table(name="states")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StateRepository")
 */
class State
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
	 * @Groups({"city", "state", "state_and_cities"})  
     */
	private $id;
	
	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
	 * @Groups({"city", "state", "state_and_cities", "store"})
     */
	private $name;

	/**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
	 * @Groups({"city", "state", "state_and_cities"})  
     */
	private $slug;
	

    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=2, nullable=false)
	 * @Groups({"city", "state", "state_and_cities", "store"})
     */
    private $abbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=false)
	 * @Groups({"city", "state", "state_and_cities"})  
     */
	private $region;
	
    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="state")	  
     */
	private $cities;
		
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return State
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     *
     * @return State
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return State
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return State
     */
    public function addCity(\AppBundle\Entity\City $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \AppBundle\Entity\City $city
     */
    public function removeCity(\AppBundle\Entity\City $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return State
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
