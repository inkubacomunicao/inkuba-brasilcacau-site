<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Theme
 *
 * @ORM\Table(name="themes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThemeRepository")
 * @UniqueEntity(
 *     fields={"slug"},
 *     errorPath="slug",
 *     message="Este tema já está cadastrada"
 * ) 
 */
class Theme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=64, nullable=false, unique=true)
	 * @Assert\NotNull()
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=45, nullable=false)
     * @Assert\Regex(
     *     pattern="/[a-z0-9\-]+/",
     *     match=true,
     * )
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="text_color", type="string", length=64, nullable=true)
	 * @Assert\NotNull()
     * @Assert\Regex(
     *      pattern="/^#[0-9A-Fa-f]{6}$/",
     *      match=true,
     *      message="O valor deve ser uma cor hexadecimal válida."
     * )* 
     */
    private $textColor;

    /**
     * @var string
     *
     * @ORM\Column(name="page_background_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=1024, maxWidth=2560, minHeight=480, maxHeight=2560)
     */
    private $pageBackgroundImage;

    /**
     * @var string
     *
     * @ORM\Column(name="text_background_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=1024, maxWidth=2560, minHeight=400, maxHeight=2560)
     */
    private $textBackgroundImage;    

    /**
     * @var string
     *
     * @ORM\Column(name="title_background_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=30, maxWidth=480, minHeight=30, maxHeight=480)
     */
    private $titleBackgroundImage;        

    /**
     * @var string
     *
     * @ORM\Column(name="tl_parallax_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=30, maxWidth=480, minHeight=30, maxHeight=480)
     */
    private $topLeftParallaxImage;     

    /**
     * @var string
     *
     * @ORM\Column(name="bl_parallax_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=30, maxWidth=480, minHeight=30, maxHeight=480)
     */
    private $bottomLeftParallaxImage;        
    
    /**
     * @var string
     *
     * @ORM\Column(name="tr_parallax_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=30, maxWidth=480, minHeight=30, maxHeight=480)
     */
    private $topRightParallaxImage;        

    /**
     * @var string
     *
     * @ORM\Column(name="br_parallax_image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=30, maxWidth=480, minHeight=30, maxHeight=480)
     */
    private $bottomRightParallaxImage; 

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="theme")
     */
	private $products;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
		$this->createdAt = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set text
     *
     * @param string $text
     *
     * @return Theme
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tag
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return Theme
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
        $product->setTheme($this);

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Get the value of pageBackgroundImage
     *
     * @return  string
     */ 
    public function getPageBackgroundImage()
    {
        return $this->pageBackgroundImage;
    }

    /**
     * Set the value of pageBackgroundImage
     *
     * @param  string  $pageBackgroundImage
     *
     * @return  self
     */ 
    public function setPageBackgroundImage($pageBackgroundImage)
    {
        $this->pageBackgroundImage = $pageBackgroundImage;

        return $this;
    }

    /**
     * Get the value of textBackgroundImage
     *
     * @return  string
     */ 
    public function getTextBackgroundImage()
    {
        return $this->textBackgroundImage;
    }

    /**
     * Set the value of textBackgroundImage
     *
     * @param  string  $textBackgroundImage
     *
     * @return  self
     */ 
    public function setTextBackgroundImage($textBackgroundImage)
    {
        $this->textBackgroundImage = $textBackgroundImage;

        return $this;
    }

    /**
     * Get the value of topLeftParallaxImage
     *
     * @return  string
     */ 
    public function getTopLeftParallaxImage()
    {
        return $this->topLeftParallaxImage;
    }

    /**
     * Set the value of topLeftParallaxImage
     *
     * @param  string  $topLeftParallaxImage
     *
     * @return  self
     */ 
    public function setTopLeftParallaxImage($topLeftParallaxImage)
    {
        $this->topLeftParallaxImage = $topLeftParallaxImage;

        return $this;
    }

    /**
     * Get the value of bottomLeftParallaxImage
     *
     * @return  string
     */ 
    public function getBottomLeftParallaxImage()
    {
        return $this->bottomLeftParallaxImage;
    }

    /**
     * Set the value of bottomLeftParallaxImage
     *
     * @param  string  $bottomLeftParallaxImage
     *
     * @return  self
     */ 
    public function setBottomLeftParallaxImage($bottomLeftParallaxImage)
    {
        $this->bottomLeftParallaxImage = $bottomLeftParallaxImage;

        return $this;
    }

    /**
     * Get the value of topRightParallaxImage
     *
     * @return  string
     */ 
    public function getTopRightParallaxImage()
    {
        return $this->topRightParallaxImage;
    }

    /**
     * Set the value of topRightParallaxImage
     *
     * @param  string  $topRightParallaxImage
     *
     * @return  self
     */ 
    public function setTopRightParallaxImage($topRightParallaxImage)
    {
        $this->topRightParallaxImage = $topRightParallaxImage;

        return $this;
    }

    /**
     * Get the value of bottomRightParallaxImage
     *
     * @return  string
     */ 
    public function getBottomRightParallaxImage()
    {
        return $this->bottomRightParallaxImage;
    }

    /**
     * Set the value of bottomRightParallaxImage
     *
     * @param  string  $bottomRightParallaxImage
     *
     * @return  self
     */ 
    public function setBottomRightParallaxImage($bottomRightParallaxImage)
    {
        $this->bottomRightParallaxImage = $bottomRightParallaxImage;

        return $this;
    }

    /**
     * Get the value of titleBackgroundImage
     *
     * @return  string
     */ 
    public function getTitleBackgroundImage()
    {
        return $this->titleBackgroundImage;
    }

    /**
     * Set the value of titleBackgroundImage
     *
     * @param  string  $titleBackgroundImage
     *
     * @return  self
     */ 
    public function setTitleBackgroundImage($titleBackgroundImage)
    {
        $this->titleBackgroundImage = $titleBackgroundImage;

        return $this;
    }

    /**
     * Get the value of textColor
     *
     * @return  string
     */ 
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * Set the value of textColor
     *
     * @param  string  $textColor
     *
     * @return  self
     */ 
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Theme
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Theme
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return Theme
     */
    public function setCulture(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
