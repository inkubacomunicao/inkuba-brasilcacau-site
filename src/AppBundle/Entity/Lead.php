<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AssertCustom;

/**
 * Lead
 *
 * @ORM\Table(name="leads")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeadRepository")
 */
class Lead
{
    /**
     * @var guid
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="campaign", type="string", length=45, nullable=true)
     * @Groups({"lead"})
     */
    private $campaign;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=90, nullable=false)
     * @Assert\NotBlank(
     *      message = "Digite sem nome completo."
     * )
     * @Groups({"lead"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=14, nullable=true)
     * @AssertCustom\IsBrazilianDocument()
     * @Groups({"lead"})
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=9, nullable=true)
     * @Assert\Regex(
     *      pattern="/[0-9]{5}-[0-9]{3}$/",
     *      match=true,
     *      message="CEP deve estar no formato válido."
     * )
     * @Groups({"lead"})
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=90, nullable=true)
     * @Groups({"lead"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=45, nullable=true)
     * @Groups({"lead"})
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="complement", type="string", length=45, nullable=true)
     * @Groups({"lead"})
     */
    private $complement;

    /**
     * @var string
     *
     * @ORM\Column(name="neighborhood", type="string", length=45, nullable=true)
     * @Groups({"lead"})
     */
    private $neighborhood;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=false)
     * @Assert\NotBlank(
     *      message = "Preencha com seu e-mail de contato."
     * )
     * @Assert\Email(
     *      message = "O e-mail deve ser válido.",
     *      checkMX = true
     * )
     * @Groups({"lead"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phonePrimary", type="string", length=17, nullable=true)
     * @Assert\Regex(
     *      pattern="/(\([0-9]{2}\))\s([9]{1})?([0-9]{4})-([0-9]{4})/",
     *      match=true,
     *      message="Telefone inválido. Ex: (99) 9999-9999"
     * )
     * @Groups({"lead"})
     */
    private $phonePrimary;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneSecondary", type="string", length=17, nullable=true)
     * @Assert\Regex(
     *      pattern="/(\([0-9]{2}\))\s([9]{1})?([0-9]{4})-([0-9]{4})/",
     *      match=true,
     *      message="Preencha com um telefone válido."
     * )
     * @Groups({"lead"})
     */
    private $phoneSecondary;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_agreed", type="boolean", nullable=true)
     */
    private $hasAgreed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->hasAgreed = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Lead
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set campaign
     *
     * @param string $campaign
     *
     * @return Lead
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return string
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set cpf
     *
     * @param string $cpf
     *
     * @return Lead
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return Lead
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Lead
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Lead
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set complement
     *
     * @param string $complement
     *
     * @return Lead
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * Get complement
     *
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * Set neighborhood
     *
     * @param string $neighborhood
     *
     * @return Lead
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * Get neighborhood
     *
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Lead
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phonePrimary
     *
     * @param string $phonePrimary
     *
     * @return Lead
     */
    public function setPhonePrimary($phonePrimary)
    {
        $this->phonePrimary = $phonePrimary;

        return $this;
    }

    /**
     * Get phonePrimary
     *
     * @return string
     */
    public function getPhonePrimary()
    {
        return $this->phonePrimary;
    }

    /**
     * Set phoneSecondary
     *
     * @param string $phoneSecondary
     *
     * @return Lead
     */
    public function setPhoneSecondary($phoneSecondary)
    {
        $this->phoneSecondary = $phoneSecondary;

        return $this;
    }

    /**
     * Get phoneSecondary
     *
     * @return string
     */
    public function getPhoneSecondary()
    {
        return $this->phoneSecondary;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Lead
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Lead
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set hasAgreed
     *
     * @param boolean $hasAgreed
     *
     * @return User
     */
    public function setHasAgreed($hasAgreed)
    {
        $this->hasAgreed = $hasAgreed;

        return $this;
    }

    /**
     * Get hasAgreed
     *
     * @return boolean
     */
    public function getHasAgreed()
    {
        return $this->hasAgreed;
    }
}
