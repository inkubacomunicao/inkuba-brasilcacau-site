<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class BannerDatatable
 *
 * @package AppBundle\Datatables
 */
class BannerDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
			'type' => 'POST',
		));

        $this->options->set(array(
			'order' => array(array(3, 'desc')),	
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
			'order_cells_top' => false,
			'classes' => Style::BOOTSTRAP_3_STYLE,
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
				'title' => 'Id',
				'width' => '20',				
                ))
			->add('desktopImage', ImageColumn::class, array(
				'title' => 'Imagem Desktop',
				'imagine_filter' => 'manager_datatable_thumb',
				'imagine_filter_enlarged' => 'manager_datatable_preview',
				'relative_path' => $options['imagePath'],
				'holder_url' => 'http://via.placeholder.com/80x45',
				'enlarge' => true,
				))
            ->add('mobileImage', ImageColumn::class, array(
                'title' => 'Imagem Mobile',
                'imagine_filter' => 'manager_datatable_thumb',
                'imagine_filter_enlarged' => 'manager_datatable_preview',
                'relative_path' => $options['imagePath'],
                'holder_url' => 'http://via.placeholder.com/80x45',
                'enlarge' => true,
            ))
            ->add('ordenator', Column::class, array(
				'title' => 'Prioridade',
				'width' => '20',				
            ))        
            ->add('publishAt', DateTimeColumn::class, array(
				'title' => 'Publicação',
				'class_name' => 'visible-lg',				
				'width' => '120',
				'date_format' => 'DD-MM-YYYY HH:mm',
            ))	                                
            ->add('expiresAt', DateTimeColumn::class, array(
				'title' => 'Expiração',
				'class_name' => 'visible-lg',				
				'width' => '120',
				'date_format' => 'DD-MM-YYYY HH:mm',
			))            
            ->add('isActive', BooleanColumn::class, array(
                'title' => 'Ativo',
                'width' => '60',
                'true_label' => 'Sim',
                'false_label' => 'Não'
            ))
            ->add(null, ActionColumn::class, array(
                'title' => 'Ações',
                'width' => '140',
                'class_name' => 'text-center',
                'actions' => array(
                    array(
                        'route' => 'manager_banner_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-arrow-up',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Subir',
                            'class' => 'btn btn-primary btn-xs ordenator',
							'role' => 'button',
							'data-direction' => 'up'
                        ),		
						'render_if' => function ($row) {
							return $row['ordenator'] < $this->em->getRepository("AppBundle:Banner")->findMaxOrdenator();
						},	                          			
                    ),
                    array(
                        'icon' => 'glyphicon glyphicon-arrow-up',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Subir',
                            'class' => 'btn disabled btn-primary btn-xs ordenator',
							'role' => 'button',
							'data-direction' => 'up'
                        ),		
						'render_if' => function ($row) {
							return $row['ordenator'] >= $this->em->getRepository("AppBundle:Banner")->findMaxOrdenator();
						},	                          			
                    ),    
                    array(
                        'icon' => 'glyphicon glyphicon-arrow-down',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Descer',
                            'class' => 'btn disabled btn-primary btn-xs ordenator',
							'role' => 'button',
							'data-direction' => 'down'
                        ),			
						'render_if' => function ($row) {
							return $row['ordenator'] <= 0;
						},	                        		
                    ),                                    
                    array(
                        'route' => 'manager_banner_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),                        
                        'icon' => 'glyphicon glyphicon-arrow-down',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Descer',
                            'class' => 'btn btn-primary btn-xs ordenator',
							'role' => 'button',
							'data-direction' => 'down'
                        ),			
						'render_if' => function ($row) {
							return $row['ordenator']  > 0;
						},	                        		
                    ),
                    array(
                        'route' => 'manager_banner_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-hand-up',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Clique para Ativar',
                            'class' => 'btn btn-danger btn-xs status',
							'role' => 'button',
							'data-status' => '1'
						),
						'render_if' => function ($row) {
                            return boolval(preg_match('/Não/', $row['isActive'], $matches));
						},						
                    ),						
                    array(
                        'route' => 'manager_banner_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-hand-down',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Clica para Inativar',
                            'class' => 'btn btn-success btn-xs status',
							'role' => 'button',
							'data-status' => '0'
						),
						'render_if' => function ($row) {
							return boolval(preg_match('/Sim/', $row['isActive'], $matches));
						},					
                    ),                    
                    array(
                        'route' => 'manager_banner_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Mostrar',
                            'class' => 'btn btn-default btn-xs',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'manager_banner_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Editar',
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ));
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Banner';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'banner_datatable';
    }
}
