<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class StoreDatatable
 *
 * @package AppBundle\Datatables
 */
class StoreDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
			'type' => 'POST',
		));

        $this->options->set(array(
			'order' => array(array(1, 'asc')),	
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
			'order_cells_top' => true,
			'classes' => Style::BOOTSTRAP_3_STYLE,
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
				'title' => 'Id',
				'width' => '20',
                ))
            ->add('name', Column::class, array(
				'title' => 'Nome',
                ))
            ->add('city.name', Column::class, array(
                'title' => 'Cidade',
                'width' => '200',
            ))
            ->add('city.state.abbreviation', Column::class, array(
                'title' => 'Estado',
                'width' => '20',
            ))
            ->add('isActive', BooleanColumn::class, array(
                'title' => 'Ativa',
                'width' => '60',
                'true_label' => 'Sim',
                'false_label' => 'Não',
                'default_content' => 'Pending',
            ))
            ->add(null, ActionColumn::class, array(
				'title' => 'Ações',
				'width' => '100',
				'class_name' => 'text-center',
                'actions' => array(
                    array(
                        'route' => 'manager_store_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
							'title' => 'Mostrar',
							'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Store';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'store_datatable';
    }
}
