<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class ProductInfoDatatable
 *
 * @package AppBundle\Datatables
 */
class ProductInfoDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
			'type' => 'POST',
		));

        $this->options->set(array(
			'order' => array(array(0, 'asc')),	
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
			'order_cells_top' => true,
			'classes' => Style::BOOTSTRAP_3_STYLE,
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
				'title' => 'Id',
				'width' => '20',					
                ))
            ->add('title', Column::class, array(
                'title' => 'Título',
				))
            ->add('isActive', BooleanColumn::class, array(
				'title' => 'Ativa',
				'width' => '60',
				'true_label' => 'Sim',
				'false_label' => 'Não',
				'default_content' => 'Pending',										
                ))
            ->add(null, ActionColumn::class, array(
                'title' => 'Ações',
				'width' => '100',
				'class_name' => 'text-center',	
				'actions' => array(
                    array(
                        'route' => 'manager_product_info_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
							'title' => 'Mostrar',
							'class' => 'btn btn-default btn-xs',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'manager_product_info_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
							'rel' => 'tooltip',							
                            'title' => 'Editar',
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\ProductInfo';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_info_datatable';
    }
}
