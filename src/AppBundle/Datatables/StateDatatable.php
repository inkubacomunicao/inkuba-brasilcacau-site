<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class StateDatatable
 *
 * @package AppBundle\Datatables
 */
class StateDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
			'type' => 'POST',
		));

        $this->options->set(array(
			'order' => array(array(1, 'asc')),	
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
			'order_cells_top' => true,
			'classes' => Style::BOOTSTRAP_3_STYLE,
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
				'title' => 'Id',
				'width' => '20',
                ))
            ->add('name', Column::class, array(
				'title' => 'Estado',
                ))
            ->add('abbreviation', Column::class, array(
				'title' => 'Sigla',
				'width' => '100',
                ))
            ->add('region', Column::class, array(
				'title' => 'Região',
				'width' => '100',
                ))
			->add('cities', Column::class, array(
				'title' => 'Cidades',
				'width' => '100',
				'dql' => '(SELECT COUNT({c}) FROM AppBundle:City {c} WHERE {c}.state = state)',
				'orderable' => true,
				))
            ->add(null, ActionColumn::class, array(
				'title' => 'Ações',
				'width' => '100',
				'class_name' => 'text-center',
                'actions' => array(
                    array(
                        'route' => 'manager_state_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
							'title' => 'Mostrar',
							'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\State';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'state_datatable';
    }
}
