<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class LeadDatatable
 *
 * @package AppBundle\Datatables
 */
class LeadDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
			'type' => 'POST',
		));

        $this->options->set(array(
			'order' => array(array(0, 'desc')),			
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
			'order_cells_top' => true,
			'classes' => Style::BOOTSTRAP_3_STYLE,
        ));

        $this->features->set(array(
		));
		
		$this->extensions->set(array(
			'buttons' => array(
				'show_buttons' => array('csv', 'excel'),
			)
		));		

        $this->columnBuilder
            ->add('id', Column::class, array(
				'title' => 'Id',
				'width' => '230',				
                ))
            ->add('campaign', Column::class, array(
                'title' => 'Campanha',
                'default_content' => 'N/D'
                ))                
            ->add('name', Column::class, array(
                'title' => 'Nome',
                ))
            ->add('email', Column::class, array(
                'title' => 'E-mail',
                'visible' => false,
                'searchable' => true,
                ))
            ->add('phonePrimary', Column::class, array(
                'title' => 'Contato Primário',
                'visible' => false,
                'searchable' => true,
                ))
            ->add('phoneSecondary', Column::class, array(
                'title' => 'Contato Secundário',
                'visible' => false,
                'searchable' => true,
                ))                 
            ->add('contact', VirtualColumn::class, array(
                'title' => 'Contatos',
                ))                 
            ->add('createdAt', DateTimeColumn::class, array(
				'title' => 'Data',
				'width' => '120',	
				'date_format' => 'YYYY-MM-DD HH:mm',
                ))
            ->add(null, ActionColumn::class, array(
				'title' => 'Ações',
				'class_name' => 'text-center',				
				'width' => '100',				
                'actions' => array(
                    array(
                        'route' => 'manager_lead_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
							'title' => $this->translator->trans('sg.datatables.actions.show'),
							'class' => 'btn btn-defatult btn-xs',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    public function getLineFormatter()
    {
        $formatter = function($line) {
            $email = $line['email'] ? sprintf('<a target="_blank" href="mailto:%s">%s</a><br />', $line['email'], $line['email']) : '';
            $phonePrimary = $line['phonePrimary'] ? sprintf('%s<br />', $line['phonePrimary']) : '';
            $phoneSecondary = $line['phoneSecondary'] ? sprintf('%s<br />', $line['phoneSecondary']) : '';
            
            $line['contact'] = sprintf('%s%s%s', $email, $phonePrimary, $phoneSecondary); 
            $line['contact'] = strlen(trim($line['contact'])) > 0 ?  trim($line['contact']) : 'N/D';

            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Lead';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lead_datatable';
    }
}
