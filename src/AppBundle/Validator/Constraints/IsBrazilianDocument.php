<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsBrazilianDocument extends Constraint
{
    public $cpf = false;
    public $cnpj = false;
    public $mask = false;
    public $messageMask = 'O {{ type }} não está em um formato válido.';
    public $message = 'O {{ type }} deve conter números formatados e ser válido.';
}
