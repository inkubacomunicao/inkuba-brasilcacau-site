<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Entity\User;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        
        $user->setUsername('cbc');
        $user->setRoles(['ROLE_MANAGER']);
        $user->setIsActive(true);
        $user->setCreatedAt(new \DateTime);

        $user->setPassword($this->encoder->encodePassword($user, 'mudar@123'));
        $manager->persist($user);
        $manager->flush();
    }
}
