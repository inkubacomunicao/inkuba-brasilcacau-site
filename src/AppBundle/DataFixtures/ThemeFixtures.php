<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use AppBundle\Service\SlugGenerator;
use AppBundle\Entity\Theme;

class ThemeFixtures extends Fixture implements DependentFixtureInterface
{
    private $slugify;

    public function __construct(SlugGenerator $slugify)
    {
        $this->slugify = $slugify;
    }

    public function load(ObjectManager $manager)
    {
		$items_array = [
			['Trufas','Trufas que se desfazem na boca e em uma variedade de sabores para encher as mãos.'],
            ['Minitrufas','Criadas especialmente para os amantes de chocolate que apreciam um produto diferenciado, as minitrufas são deliciosas doses de requinte que derretem na boca.'],
            ['Dinda','Dinda é o mais fofinho marshmallow coberto com chocolate. Agora em uma variedade de sabores. Vai ser difícil dividir com alguém!'],
            ['Tabletes','O puro sabor do chocolate em deliciosas variações.'],
			['Presentes','Para surpreender, comemorar ou agradar em todos os momentos. Seja qual for a ocasião, o presente perfeito está aqui.'],
			['Infantil','Diversidade em chocolates para a criançada se divertir e aprender. Tem coisa melhor do que brincar e descobrir ao mesmo tempo novos sabores, texturas e aromas?'],
			['Diet','Para aquelas pessoas que amam chocolate, mas tem ingestão controlada de açúcar, a Chocolates Brasil Cacau pensou em vocês!'],
			['Variedades', 'Chocolates clássicos e sabores de sucesso você encontra aqui. São irresistíveis e perfeitos para acompanhamento!'],
            ['Cafeteria','Que tal uma bebida feita com o nosso irresistível chocolate? Saborear um expresso ou cappuccino nunca foi tão delicioso.'],
            ['Acessórios','A Chocolates Brasil Cacau tem a melhor opção para você presentear alguém especial! Se o chocolate já é bom, imagine acompanhado de um lindo acessório. Escolha o seu!']
        ];

        foreach ($items_array as $item_array) {
			$item = new Theme();

			$item->setText($item_array[0]);
            $item->setDescription($item_array[1]);
			$item->setSlug($this->slugify->slugify($item_array[0]));
            $item->setIsActive(true);

            $manager->persist($item);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
			StateFixtures::class,
		];
    }    
}