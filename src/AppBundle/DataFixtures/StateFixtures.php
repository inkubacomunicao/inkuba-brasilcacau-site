<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Service\SlugGenerator;
use AppBundle\Entity\State;

class StateFixtures extends Fixture
{
    private $slugify;

    public function __construct(SlugGenerator $slugify)
    {
        $this->slugify = $slugify;
    }

    public function load(ObjectManager $manager)
    {
        $items_array = [
			['Acre','AC','Norte'],
			['Alagoas','AL','Nordeste'],
			['Amapá','AP','Norte'],
			['Amazonas','AM','Norte'],
			['Bahia','BA','Nordeste'],
			['Ceará','CE','Nordeste'],
			['Distrito Federal','DF','Centro-Oeste'],
			['Espírito Santo','ES','Sudeste'],
			['Goiás','GO','Centro-Oeste'],
			['Maranhão','MA','Nordeste'],
			['Mato Grosso','MT','Centro-Oeste'],
			['Mato Grosso do Sul','MS','Centro-Oeste'],
			['Minas Gerais','MG','Sudeste'],
			['Pará','PA','Norte'],
			['Paraíba','PB','Nordeste'],
			['Paraná','PR','Sul'],
			['Pernambuco','PE','Nordeste'],
			['Piauí','PI','Nordeste'],
			['Rio de Janeiro','RJ','Sudeste'],
			['Rio Grande do Norte','RN','Nordeste'],
			['Rio Grande do Sul','RS','Sul'],
			['Rondônia','RO','Norte'],
			['Roraima','RR','Norte'],
			['Santa Catarina','SC','Sul'],
			['São Paulo','SP','Sudeste'],
			['Sergipe','SE','Nordeste'],
			['Tocantins','TO','Norte'],
		];

        foreach ($items_array as $item_array) {
            $item = new State();

			$item->setName($item_array[0]);
			$item->setAbbreviation($item_array[1]);
			$item->setRegion($item_array[2]);
			$item->setSlug($this->slugify->slugify($item_array[1]));

            $manager->persist($item);
        }

        $manager->flush();
    } 
}