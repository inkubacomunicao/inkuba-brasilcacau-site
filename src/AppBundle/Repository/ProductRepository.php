<?php

namespace AppBundle\Repository;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    public function findBySearchString($string, $limit = null, $offset = 0)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p')
            ->from('AppBundle\Entity\Product', 'p')
            ->where('p.isActive = 1');
        
        $words = explode(" ", $string);

        foreach ($words as $i => $word) {
            if ($i < 5) {
                if (strlen($word) >= 3) {
                    $qb->andWhere($qb->expr()->orX($qb->expr()->like('p.name', ':word_'.$i),$qb->expr()->like('p.description', ':word_'.$i)))->setParameter(':word_'.$i, '%'.$word.'%');
                }
            } else {
                break;
            }
        }

        $qb->setFirstResult($offset)
           ->setMaxResults($limit);

        $results = $qb->getQuery()->getResult();

        return $results;
    }

    public function findByCategory($category, $limit = null, $offset = 0)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT p 
                            FROM AppBundle\Entity\Product p 
                            WHERE :category MEMBER OF p.categories and p.isActive = 1 
                            ORDER BY p.name  ASC')
            ->setParameter('category', $category)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();
    }    
}
