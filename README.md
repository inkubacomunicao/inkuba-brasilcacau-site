inkuba-brasilcacau-site
=======================

A Symfony project created on August 7, 2018, 11:04 am.

Install
====
Run: npm install (development only)
Run: composer install

Setup your parameters.yml accordingly.

Run: php bin/console doctrine:database:drop --force (ATTENTION: Only necessary if replacing existing DB. ALL DATA WILL BE ERASED.)
Run: php bin/console doctrine:database:create
Run: php bin/console doctrine:schema:update --force
Run: php bin/console doctrine:fixtures:load

Run
====
Run: php bin/console server:run (development only)
Run: gulp (development only)

Server - Clear cache
====
Run: cd /var/www/html/chocolatesbrasilcacau.com.br/www/
Run: php72 bin/console cache:clear --env=prod
Run: php bin/console cache:clear --env=prod
