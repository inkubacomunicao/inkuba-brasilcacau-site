var gulp = require('gulp');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var flatmap = require('gulp-flatmap');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
	browserSync.init({
		proxy: '127.0.0.1:8000'
	});
});

gulp.task('bs-reload', function () {
	browserSync.reload();
});

gulp.task('images', function(){
	gulp.src('web/img/**/*')
		.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
		.pipe(gulp.dest('dist/images/'));
});

gulp.task('styles', function(){
	gulp.src(['app/Resources/public/site/sass/**/*.scss'])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(sass())
		.pipe(autoprefixer('last 2 versions'))
		.pipe(sass({errLogToConsole: true}))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('web/assets/site/css/'))
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('scripts', function(){
	gulp.src(['app/Resources/public/site/scripts/**/*.js'])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('web/assets/site/js/'))
		.pipe(browserSync.reload({stream:true}))

	gulp.src(['app/Resources/public/manager/scripts/**/*.js'])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(rename({suffix: '.min'}))
		//.pipe(uglify())
		.pipe(gulp.dest('web/assets/manager/js/'))
		.pipe(browserSync.reload({stream:true}))		
});

gulp.task('default', ['browser-sync'], function(){
	gulp.watch("app/Resources/public/site/sass/**/*.scss", ['styles']);
	gulp.watch("app/Resources/public/site/scripts/**/*.js", ['scripts']);
	gulp.watch("app/Resources/public/manager/scripts/**/*.js", ['scripts']);
	gulp.watch("app/Resources/views/**", ['bs-reload']);
});